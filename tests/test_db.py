import unittest
from random import randint
from datetime import datetime
import db.dbadapter
from db.models import SqlOrmBase, User, Gift, Room
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from db.dbadapter import has_active_room


class TestDBInteraction(unittest.TestCase):
    """ Has side effect: creates sqlite db file from db.config import in db.dbadapter"""
    def __init__(self, arg):
        unittest.TestCase.__init__(self, arg)
        self.db_engine = create_engine('sqlite://')
        SqlOrmBase.metadata.create_all(self.db_engine)
        self.db_session = Session(bind=self.db_engine)
        db.dbadapter.db_session = self.db_session

    def tearDown(self) -> None:
        SqlOrmBase.metadata.drop_all(self.db_engine)

    def test_has_active_room(self) -> None:
        self.assertFalse(has_active_room(randint(0, 100)))

        tg_id = randint(0, 100)
        u = User(tg_id=tg_id,  chat_id=0)
        self.db_session.add(u)
        self.db_session.commit()
        self.assertFalse(has_active_room(tg_id))

        tg_id = randint(0, 100)
        u = User(tg_id=tg_id, chat_id=1)
        r = Room(title='test title', invite_token='test token', event_date=datetime.now())
        r.list.append(u)
        u.active_room = r
        self.db_session.add(u)
        self.db_session.add(r)
        self.db_session.commit()
        self.assertTrue(has_active_room(tg_id))


if __name__ == '__main__':
    unittest.main()
