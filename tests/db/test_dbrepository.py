import unittest
from unittest.mock import MagicMock
from core.entities import User
from db.models import UserOrm
from db.dbrepository import DbRepository
# from sqlalchemy import create_engine
# from sqlalchemy.orm import Session

class TestDbRepository(unittest.TestCase):
    def setUp(self) -> None:
        self.session_mock = MagicMock()
        self.session_mock.add = MagicMock()
        self.session_mock.commit = MagicMock()
        self.session_mock.query = MagicMock()
        self.repository = DbRepository(self.session_mock)
        self.filter_mock = MagicMock()
        self.filter_mock.filter = MagicMock()
        self.return_query_mock = MagicMock()
        self.return_query_mock.first = MagicMock()
        self.filter_mock.filter.return_value = self.return_query_mock

    def test_add_user(self) -> None:
        user = User(1, 2, 'test')
        self.assertTrue(self.repository.create_user(user))
        self.session_mock.commit.assert_called_once()
        self.session_mock.query = MagicMock()
        orm = self.session_mock.add.call_args.args[0]
        self.assertEqual((user.tg_id, user.chat_id, user.name), (orm.tg_id, orm.chat_id, orm.name))

    def test_update_user(self) -> None:
        self.assertFalse(self.repository.update_user(User(1,2,'test')))

        self.repository.__avatar_from_orm = MagicMock()
        self.repository.__avatar_from_orm.return_value = None
        user_id, tg_id, chat_id = range(3)
        user_name = 'test'

        self.return_query_mock.first.return_value = UserOrm(id=user_id, tg_id=tg_id, chat_id=chat_id, name=user_name)
        self.session_mock.query.return_value = self.filter_mock

        user = User(id=user_id, tg_id=tg_id, chat_id=chat_id, name=user_name)
        self.assertTrue(self.repository.update_user(user))
        self.session_mock.add.assert_not_called()
        self.session_mock.commit.assert_not_called()

        self.return_query_mock.first.return_value = UserOrm(id=user_id, tg_id=33, chat_id=44, name='test2')
        self.assertTrue(self.repository.update_user(user))
        self.session_mock.add.assert_called_once()
        self.session_mock.commit.assert_called_once()

    def test_get_user(self):
        self.return_query_mock.first.return_value = None
        self.session_mock.query.return_value = self.filter_mock
        self.assertFalse(self.repository.user(0))

        user_id, tg_id, chat_id = range(3)
        user_name = 'test'
        orm = UserOrm(id=user_id, tg_id=tg_id, chat_id=chat_id, name=user_name)

        self.repository.__avatar_from_orm = MagicMock()
        self.repository.__avatar_from_orm.return_value = None
        self.return_query_mock.first.return_value = orm
        user = self.repository.user(0)
        self.assertEqual((user.id, user.tg_id, user.chat_id, user.name),
                         (orm.id, orm.tg_id, orm.chat_id, orm.name))