import unittest
from unittest.mock import MagicMock
from core.user import User, UserManager
import random


class TestUserManager(unittest.TestCase):
    def setUp(self) -> None:
        self.repo_mock = MagicMock()
        self.repo_mock.user = MagicMock()
        self.repo_mock.update_user = MagicMock()
        self.repo_mock.add_user = MagicMock()
        self.manager = UserManager(self.repo_mock)

    def test_add_user(self) -> None:
        self.repo_mock.user.return_value = None
        self.repo_mock.add_user.return_value = True
        user = User(1, 2, 'test')

        self.assertTrue(self.manager.validate_user(user))
        self.repo_mock.user.assert_called_once()
        self.repo_mock.add_user.assert_called_with(user)

    def test_update_user(self) -> None:
        self.repo_mock.update_user.return_value = True
        self.repo_mock.user.return_value = User(1, 2, 'test user', 22, [1, 2])
        user = User(1, 13, 'new name')

        self.assertTrue(self.manager.validate_user(user))
        self.repo_mock.update_user.assert_called_with(User(1, 13, 'new name', 22, [1, 2]))

    def test_set_active_room(self) -> None:
        self.repo_mock.user.return_value = None
        self.assertFalse(self.manager.set_active_room(0, 1))

        self.repo_mock.update_user.return_value = True
        self.repo_mock.user.return_value = User(0, 1, 'test', 2)
        room_id = random.randint(0, 100)
        self.assertTrue(self.manager.set_active_room(0, room_id))
        self.repo_mock.update_user.assert_called_with(User(0, 1, 'test', room_id))


    def test_add_room(self):
        self.repo_mock.user.return_value = None
        self.assertFalse(self.manager.add_room(0, 1))

        self.repo_mock.update_user.return_value = True
        self.repo_mock.user.return_value = User(0, 1, 'test', 2)
        room_id = random.randint(0, 30)
        self.assertTrue(self.manager.add_room(0, room_id))
        user = User(0, 1, 'test', 2, [room_id])
        self.repo_mock.update_user.assert_called_with(user)

        self.repo_mock.user.return_value = user
        room_id = random.randint(31, 100)
        self.assertTrue(self.manager.add_room(0, room_id))
        self.assertTrue(len(user.rooms) == 2 and room_id in user.rooms)

    def test_remove_room(self):
        self.repo_mock.user.return_value = None
        self.assertFalse(self.manager.remove_room(0, 1))

        self.repo_mock.update_user.return_value = True
        self.repo_mock.user.return_value = User(0, 1, 'test')
        self.assertFalse(self.manager.remove_room(0, 1))

        ids = [2, 3, 4, 5]
        user = User(0, 1, 'test', 11, ids)
        self.repo_mock.user.return_value = user
        self.assertFalse(self.manager.remove_room(0, 1))

        room_id = random.choice(ids)
        self.assertTrue(self.manager.remove_room(0, room_id))
        self.assertTrue(room_id not in user.rooms)