import unittest
from unittest.mock import MagicMock
from core.gift import GiftManager, Gift, Filter
import random

class TestGiftManager(unittest.TestCase):
    def setUp(self) -> None:
        self.repo_mock = MagicMock()
        self.repo_mock.gifts = MagicMock()
        self.manager = GiftManager(self.repo_mock)

    def test_give_list(self) -> None:
        giver_id = random.randint(0, 10)
        gift = Gift(owner_id=0, giver_ids=[giver_id])
        self.repo_mock.gifts.side_effect = lambda filters: [gift] if filters.giver_id in gift.giver_ids else []
        self.assertFalse(self.manager.give_list(random.randint(10, 15)))
        self.assertEqual([gift], self.manager.give_list(giver_id))

    def test_wish_list(self) -> None:
        owner_id = random.randint(0, 10)
        gift = Gift(owner_id=owner_id)
        self.repo_mock.gifts.side_effect = lambda filters: [gift] if filters.owner_id == gift.owner_id else []
        self.assertFalse(self.manager.give_list(random.randint(10, 15)))
        self.assertEqual([gift], self.manager.wish_list(owner_id))
