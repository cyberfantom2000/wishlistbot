import unittest
from unittest.mock import MagicMock
from core.avatar import AvatarManager, Avatar
import random


class TestAvatarManager(unittest.TestCase):
    def setUp(self) -> None:
        self.repo_mock = MagicMock()
        self.repo_mock.create_avatar = MagicMock()
        self.repo_mock.remove_avatar = MagicMock()
        self.repo_mock.add_give_gift = MagicMock()
        self.repo_mock.add_wish_gift = MagicMock()
        self.repo_mock.remove_give_gift = MagicMock()
        self.repo_mock.remove_wish_gift = MagicMock()
        self.repo_mock.avatar = MagicMock()
        self.manager = AvatarManager(self.repo_mock)

    def test_add_give_gift(self) -> None:
        avatar_id = random.randint(1, 10)
        avatar = Avatar(0, avatar_id)
        self.repo_mock.avatar.side_effect = lambda i: avatar if i == avatar_id else None
        self.assertFalse(self.manager.add_give_gift(0, 1))

        self.repo_mock.update_avatar.return_value = True
        self.manager.add_give_gift(avatar_id, 1)
        self.assertTrue(len(avatar.give_list_ids) == 1)

        self.manager.add_give_gift(avatar_id, 2)
        self.assertTrue(len(avatar.give_list_ids) == 2)

    def test_add_wish_gift(self) -> None:
        avatar_id = random.randint(1, 10)
        avatar = Avatar(0, avatar_id)
        self.repo_mock.avatar.side_effect = lambda i: avatar if i == avatar_id else None
        self.assertFalse(self.manager.add_wish_gift(0, 1))

        self.repo_mock.update_avatar.return_value = True
        self.manager.add_wish_gift(avatar_id, 1)
        self.assertTrue(len(avatar.wish_list_ids) == 1)

        self.manager.add_wish_gift(avatar_id, 2)
        self.assertTrue(len(avatar.wish_list_ids) == 2)

    def test_remove_give_gift(self) -> None:
        avatar_id = random.randint(1, 10)
        avatar = Avatar(0, id=avatar_id)
        self.repo_mock.avatar.side_effect = lambda i: avatar if i == avatar_id else None
        self.assertFalse(self.manager.remove_give_gift(0, 1))
        self.assertFalse(self.manager.remove_give_gift(avatar_id, 1))
        avatar.give_list_ids = [2, 3, 4]
        self.assertFalse(self.manager.remove_give_gift(avatar_id, 1))

        self.repo_mock.update_avatar.return_value = True
        gift_id = random.choice(avatar.give_list_ids)
        self.assertTrue(self.manager.remove_give_gift(avatar_id, gift_id))
        self.assertTrue(gift_id not in avatar.give_list_ids)

    def test_remove_wish_gift(self) -> None:
        avatar_id = random.randint(1, 10)
        avatar = Avatar(0, id=avatar_id)
        self.repo_mock.avatar.side_effect = lambda i: avatar if i == avatar_id else None
        self.assertFalse(self.manager.remove_wish_gift(0, 1))
        self.assertFalse(self.manager.remove_wish_gift(avatar_id, 1))
        avatar.wish_list_ids = [2, 3, 4]
        self.assertFalse(self.manager.remove_wish_gift(avatar_id, 1))

        self.repo_mock.update_avatar.return_value = True
        gift_id = random.choice(avatar.wish_list_ids)
        self.assertTrue(self.manager.remove_wish_gift(avatar_id, gift_id))
        self.assertTrue(gift_id not in avatar.wish_list_ids)
