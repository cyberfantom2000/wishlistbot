import unittest
from unittest.mock import MagicMock
from core.room import Room, RoomManager, RoomFilter
from datetime import datetime
from random import randint

class TestRoomManager(unittest.TestCase):
    def setUp(self) -> None:
        self.repo_mock = MagicMock()
        self.repo_mock.create_room = MagicMock()
        self.repo_mock.remove_room = MagicMock()
        self.repo_mock.rooms = MagicMock()
        self.repo_mock.update_room = MagicMock()
        self.manager = RoomManager(self.repo_mock)

    def test_save_room(self):
        self.repo_mock.create_room.return_value = False
        room = Room('test', 'token', datetime.now(), 'regime', 1)
        self.assertEqual(-1, self.manager.save_room(room))

        self.repo_mock.create_room.return_value = True
        room_id = randint(0, 100)
        room.id = room_id
        self.repo_mock.rooms.return_value = [room]
        self.assertEqual(room_id, self.manager.save_room(room))
        self.repo_mock.create_room.assert_called_with(room)

    def test_remove_room(self):
        owner_id, room_id = randint(0, 50), randint(51, 100)
        self.repo_mock.rooms.return_value = [
            Room('test', 'token', datetime.now(), 'regime', owner_id=owner_id, id=room_id)
        ]
        self.assertFalse(self.manager.remove(owner_id=room_id, room_id=room_id))

        self.repo_mock.remove_room.return_value = True
        self.assertTrue(self.manager.remove(owner_id=owner_id, room_id=room_id))
        self.repo_mock.remove_room.asser_called_with(room_id)

    def test_get_rooms(self):
        room_a = Room('test1', 'token1', datetime.now(), 'regime1', 1)
        self.repo_mock.rooms.side_effect = lambda filters: [room_a] if filters[0].title == 'test1' else []
        self.assertEqual(room_a, self.manager.list(RoomFilter(title='test1')))
        self.assertEqual([], self.manager.list(RoomFilter()))

        room_b = Room('test1', 'token2', datetime.now(), 'regime', 2)
        self.repo_mock.rooms.side_effect = lambda filters: [room_a, room_b] if filters[0].title == 'test1' else []
        self.assertEqual([room_a, room_b], self.manager.list(RoomFilter(title='test1')))

    def test_add_avatar(self):
        room_id = randint(1, 100)
        room = Room('test1', 'token1', datetime.now(), 'regime1', 1, id=room_id)
        self.repo_mock.rooms.side_effect = lambda filters: [room] if filters[0].room_id == room_id else []
        self.assertFalse(self.manager.add_avatar(0, 1))

        self.repo_mock.update_room.return_value = True
        self.assertTrue(self.manager.add_avatar(room_id, 1))
        self.assertTrue(1 in room.avatars)

        self.assertTrue(self.manager.add_avatar(room_id, 2))
        self.assertTrue(1 in room.avatars and 2 in room.avatars)

    def test_remove_avatar(self):
        self.manager.list = MagicMock()
        self.manager.list.return_value = None
        self.assertFalse(self.manager.remove_avatar(0, 1))

        room_id = randint(1, 100)
        room = Room('test1', 'token1', datetime.now(), 'regime1', 1, id=room_id)
        self.manager.list.return_value = room
        self.assertFalse(self.manager.remove_avatar(0, 1))

        avatar_id = randint(1, 100)
        room.avatars = [avatar_id]
        self.assertFalse(self.manager.remove_avatar(0, 1))

        self.repo_mock.update_room.return_value = True
        self.assertTrue(self.manager.remove_avatar(0, avatar_id))
        self.assertTrue(avatar_id not in room.avatars)
