import unittest
import random
from datetime import datetime
from db.models import User, Gift, Avatar, Room, SqlOrmBase

from sqlalchemy import create_engine
from sqlalchemy.orm import Session


class TestModels(unittest.TestCase):
    s = 'sadasdasdsaghqkoiqnqwweqgc'

    def __init__(self, arg) -> None:
        unittest.TestCase.__init__(self, arg)
        self.db_engine = create_engine('sqlite://')
        SqlOrmBase.metadata.create_all(self.db_engine)
        self.db_session = Session(bind=self.db_engine)

    def tearDown(self) -> None:
        SqlOrmBase.metadata.drop_all(self.db_engine)

    def test_create_avatar(self) -> None:
        tg_id = random.randint(1, 100)
        avatar = Avatar(tg_id=tg_id)
        self.assertEqual(tg_id, avatar.tg_id)

    def test_create_user(self) -> None:
        tg_id = random.randint(1, 100)
        chat_id = random.randint(1, 100)
        name = ''.join([random.choice(TestModels.s) for _ in range(10)])
        user = User(tg_id=tg_id, chat_id=chat_id, name=name)
        self.assertEqual((tg_id, chat_id, name), (user.tg_id, user.chat_id, user.name))

    def test_create_gift(self) -> None:
        title = ''.join([random.choice(TestModels.s) for _ in range(10)])
        link = ''.join([random.choice(TestModels.s) for _ in range(10)])
        description = ''.join([random.choice(TestModels.s) for _ in range(10)])
        gift = Gift(title=title, link=link, description=description)
        self.assertEqual((title, link, description), (gift.title, gift.link, gift.description))

    def test_create_room(self) -> None:
        title = ''.join([random.choice(TestModels.s) for _ in range(10)])
        invite_token = ''.join([str(i) for i in range(10)])
        room = Room(title=title, invite_token=invite_token, event_date=datetime.now())
        self.assertEqual((title, invite_token), (room.title, room.invite_token))

    def test_relationship_avatar_to_gift(self) -> None:
        giver = Avatar(tg_id=random.randint(1, 100))
        wisher = Avatar(tg_id=random.randint(1, 100))
        gift = Gift(title='test gift')
        wisher.wishlist.append(gift)
        giver.givelist.append(gift)
        self.assertTrue(wisher in gift.wishers and wisher not in gift.givers)
        self.assertTrue(giver in gift.givers and giver not in gift.wishers)

    def test_relationship_room_to_avatar(self) -> None:
        room = Room(title='test title', invite_token='test token', event_date=datetime.now())
        ids = (1, 2)
        avatar1 = Avatar(tg_id=ids[0])
        avatar2 = Avatar(tg_id=ids[1])
        room.list.append(avatar1)
        room.list.append(avatar2)

        self.db_session.add(room)
        self.db_session.add(avatar1)
        self.db_session.add(avatar2)
        self.db_session.commit()

        room = self.db_session.query(Room).first()
        for avatar in self.db_session.query(Avatar).all():
            self.assertEqual(room.id, avatar.room_id)

        for member in room.avatars:
            self.assertTrue(member.tg_id in ids)

    def test_relationship_user_to_room(self) -> None:
        user = User(tg_id=1, chat_id=2)
        room_titles = ('test title 1', 'test title 2')
        room1 = Room(title=room_titles[0], invite_token='test token 1', event_date=datetime.now())
        room2 = Room(title=room_titles[1], invite_token='test token 2', event_date=datetime.now())

        user.list.append(room1)
        user.list.append(room2)
        user.active_room = room1

        self.db_session.add(user)
        self.db_session.add(room1)
        self.db_session.add(room2)
        self.db_session.commit()

        user = self.db_session.query(User).first()
        user_rooms = [r.id for r in user.rooms]
        for room_id in self.db_session.query(Room.id).all():
            self.assertTrue(room_id[0] in user_rooms)

        room = self.db_session.query(Room.id).filter(Room.title == room_titles[0])[0]
        self.assertEqual(user.active_room_id, room.id)


if __name__ == '__main__':
    unittest.main()
