import unittest
from unittest.mock import MagicMock, PropertyMock
import handlers.base as base


class TestBaseHandlers(unittest.TestCase):
    def setUp(self) -> None:
        self.context_mock = MagicMock()
        self.update_mock = MagicMock()
        self.update_mock.callback_query.answer = MagicMock()
        self.update_mock.callback_query.edit_message_text = MagicMock()
        self.context_mock.user_data = MagicMock()
        self.update_mock.message.reply_text = MagicMock()

    def test_start(self) -> None:
        self.context_mock.user_data.get.return_value = False
        self.assertEqual(base.start(self.update_mock, self.context_mock), base.SHOWING)
        self.update_mock.message.reply_text.assert_called()
        self.context_mock.user_data.__setitem__.assert_called_with(base.START_OVER, True)

        self.context_mock.user_data.get.return_value = True
        self.assertEqual(base.start(self.update_mock, self.context_mock), base.SHOWING)
        self.update_mock.callback_query.answer.assert_called_once()
        self.update_mock.callback_query.edit_message_text.assert_called_once()

    def test_get_rules(self) -> None:
        self.assertEqual(base.get_rules(self.update_mock, self.context_mock), base.RULES)
        self.update_mock.callback_query.answer.assert_called_once()
        self.update_mock.callback_query.edit_message_text.assert_called_once()
        self.assertFalse(self.context_mock.mock_calls)

    def test_get_help(self) -> None:
        self.assertEqual(base.get_help(self.update_mock, self.context_mock), base.HELP)
        self.update_mock.callback_query.answer.assert_called_once()
        self.update_mock.callback_query.edit_message_text.assert_called_once()
        self.assertFalse(self.context_mock.mock_calls)


if __name__ == '__main__':
    unittest.main()
