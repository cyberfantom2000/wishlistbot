from core.gift import GiftManager
from core.entities import Gift


class GiftPresenter:
    def __init__(self, gift_manager: GiftManager) -> None:
        self.gift_manager = gift_manager

    def add_gift(self, avatar_id: int,  title: str, link: str = '', description: str = '') -> bool:
        gift = Gift(avatar_id=avatar_id, title=title, link=link, description=description)
        return self.gift_manager.save_gift(gift)

    def remove_gift(self, gift_id: int) -> bool:
        return self.gift_manager.remove_gift(gift_id)

    def add_giver(self, gift_id: int, giver_id: int) -> bool:
        """ Set giver for gift """
        gift = self.gift_manager.gift(gift_id)
        if gift:
            gift.giver_ids.append(giver_id)
            self.gift_manager.save_gift(gift)
        return bool(gift)

    def remove_giver(self, gift_id: int, giver_id: int) -> bool:
        """ Remove giver for gift """
        gift = self.gift_manager.gift(gift_id)
        if gift:
            gift.giver_ids.remove(giver_id)
            return self.gift_manager.save_gift(gift)
        return False

    def gifts(self, ids: list[int]) -> list[Gift]:
        return self.gift_manager.gifts(ids)
