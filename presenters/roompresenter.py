from core.entities import Room
from core.user import UserFilter, UserManager
from core.room import RoomFilter, RoomManager
from core.avatar import AvatarManager
from datetime import datetime
import secrets


class RoomPresenter:
    def __init__(self, room_manager: RoomManager, user_manager: UserManager, avatar_manager: AvatarManager) -> None:
        self.room_manager = room_manager
        self.user_manager = user_manager
        self.avatar_manager = avatar_manager

    def create_room(self, tg_id: int, title: str, regime: str, event_date: datetime) -> Room or None:
        """ Create new room. Return room if successful otherwise None. """
        users = self.user_manager.list(UserFilter(tg_id=tg_id))
        if not users or len(users) != 1:
            return None
        else:
            return self.room_manager.create(tg_id, title, regime, event_date)

    def change_token(self, room_id: int) -> str:
        """ Generate new invite token. Return new generated token """
        room = self.room(room_id)
        if not room:
            return ""
        else:
            room.token = str(secrets.token_hex(16))
            self.room_manager.update(room)
            return room.token

    def room(self, room_id: int) -> Room or None:
        """ Get room object by id. Return room if exist otherwise None. """
        rooms = self.room_manager.list(RoomFilter(room_ids=[room_id]))
        return None if not rooms else rooms[0]

    # def user_rooms(self, user_id: int) -> list[Room]:
    #     """ Get room list by user """
    #     users = self.user_manager.users(UserFilter(tg_id=user_id))
    #     return None if not users else self.room_manager.rooms(RoomFilter(room_ids=users[0].room_ids))
