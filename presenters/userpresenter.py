from core.user import UserManager, UserFilter
from core.avatar import AvatarManager, AvatarFilter
from core.room import RoomManager, RoomFilter
from core.entities import User, Room


class UserPresenter:
    """ Add user to database """
    def __init__(self, user_manager: UserManager,
                 avatar_manager: AvatarManager,
                 room_manager: RoomManager):
        self.user_manager = user_manager
        self.avatar_manager = avatar_manager
        self.room_manager = room_manager

    def user(self, tg_id: int) -> User or None:
        users = self.user_manager.list(UserFilter(tg_id=tg_id))
        return None if not users else users[0]

    def validate_user(self, tg_id: int, chat_id: int, name: str, link: str, link_name: str) -> None:
        """ Create new user or update chat_id/name"""
        users = self.user_manager.list(UserFilter(tg_id=tg_id))
        if not users:
            self.user_manager.create(tg_id, chat_id, name, link, link_name)
        else:
            origin = users[0]
            origin.chat_id = chat_id
            origin.name = name
            origin.link = link
            origin.link_name = link_name
            self.user_manager.update(origin)

    def change_active_room(self, tg_id: int, room_id: int) -> bool:
        """ Change active room and avatar """
        avatars = self.avatar_manager.list(AvatarFilter(tg_id=tg_id, room_id=room_id))
        if not avatars:
            return False

        users = self.user_manager.list(UserFilter(tg_id=tg_id))
        if not users:
            return False
        else:
            user = users[0]
            user.active_room_id = room_id
            user.active_avatar = avatars[0]
            if room_id not in user.room_ids:
                user.room_ids.append(room_id)
            return self.user_manager.update(user)

    def enter_to_room(self, tg_id: int, token: str) -> Room or None:
        """ Add user to room by invite token """
        users = self.user_manager.list(UserFilter(tg_id=tg_id))
        if not users:
            return False

        rooms = self.room_manager.list(RoomFilter(token=token))
        if not rooms:
            return False

        users[0].room_ids.append(rooms[0].id)
        return rooms[0] if self.user_manager.update(users[0]) else None
