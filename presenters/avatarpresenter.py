from core.avatar import AvatarFilter, AvatarManager
from core.entities import Avatar


class AvatarPresenter:
    def __init__(self, avatar_manager: AvatarManager):
        self.avatar_manager = avatar_manager

    def create_avatar(self, tg_id: int, room_id: int) -> Avatar or None:
        """ Create new avatar and put it in room by room_id """
        return self.avatar_manager.create(tg_id, room_id)

    def remove_avatar(self, avatar_id: int) -> bool:
        """ Remove avatar by id """
        return self.avatar_manager.remove(avatar_id)

    def avatar(self, avatar_id: int) -> Avatar or None:
        """ Return avatar by avatar id """
        avatars = self.avatar_manager.list(AvatarFilter(avatar_id=avatar_id))
        return avatars[0] if avatars else None

    def avatar_from_room(self, owner_id: int, room_id: int):
        """ Return avatar by telegram id and room id"""
        avatars = self.avatar_manager.list(AvatarFilter(tg_id=owner_id, room_id=room_id))
        return avatars[0] if avatars else None

    def set_target_avatar(self, avatar_id: int, target_id: int) -> bool:
        avatar = self.avatar(avatar_id)
        if avatar:
            avatar.target_avatar_id = target_id
            return self.avatar_manager.update(avatar)
        else:
            return False
