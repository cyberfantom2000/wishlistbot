from apscheduler.schedulers.background import BackgroundScheduler

from handlers.base import invalid_button_handler
from bot.config import dispatcher, updater, bot
from presenters.userpresenter import UserPresenter
from presenters.roompresenter import RoomPresenter
from presenters.avatarpresenter import AvatarPresenter
from presenters.giftpresenter import GiftPresenter
from core.user import UserManager
from core.avatar import AvatarManager
from core.room import RoomManager
from core.gift import GiftManager
from db.dbrepository import DbRepository
from db.dbbase import BaseRepository
from db.config import db_session
from handlers.complexmenu import ComplexMenu
from core.roomdeleter import RoomDeleter


def main() -> None:
    base_repo = BaseRepository(db_session)
    repo = DbRepository(base_repo)

    user_manager = UserManager(repo)
    avatar_manager = AvatarManager(repo)
    room_manager = RoomManager(repo)
    gift_manager = GiftManager(repo)

    user_presenter = UserPresenter(user_manager, avatar_manager, room_manager)
    room_presenter = RoomPresenter(room_manager, user_manager, avatar_manager)
    avatar_presenter = AvatarPresenter(avatar_manager)
    gift_presenter = GiftPresenter(gift_manager)

    main_menu = ComplexMenu(user_presenter, room_presenter, avatar_presenter, gift_presenter, bot)
    conv_handler = main_menu.get_conversation_handler('start', 'stop')
    dispatcher.add_handler(conv_handler)

    dispatcher.add_handler(invalid_button_handler)

    deleter = RoomDeleter(room_manager)
    scheduler = BackgroundScheduler()
    scheduler.add_job(deleter.delete_expired_rooms, 'cron', hour='4')  # removes expired rooms on every day at 4 a.m.

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
