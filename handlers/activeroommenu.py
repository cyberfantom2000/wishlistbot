from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update, Bot
from telegram.ext import (CallbackContext,
                          CommandHandler,
                          MessageHandler,
                          Filters,
                          CallbackQueryHandler)
from handlers.defines import *
from presenters.roompresenter import RoomPresenter
from presenters.userpresenter import UserPresenter
from presenters.avatarpresenter import AvatarPresenter
from presenters.giftpresenter import GiftPresenter
from telegram import TelegramError
from core.entities import Room, User
import random


class ActiveRoomMenu:
    def __init__(self,
                 user_presenter: UserPresenter,
                 room_presenter: RoomPresenter,
                 avatar_presenter: AvatarPresenter,
                 gift_presenter: GiftPresenter,
                 bot: Bot) -> None:
        self.user_presenter = user_presenter
        self.room_presenter = room_presenter
        self.avatar_presenter = avatar_presenter
        self.gift_presenter = gift_presenter
        self.bot = bot
        self.back_func = None

    def get_conversation_handler(self, entry_state: str, cmd_stop: str, back_func, back_state: str) -> ConversationHandler:
        self.back_func = back_func

        notify_conv = ConversationHandler(
            entry_points=[CallbackQueryHandler(self._ask_input_msg, pattern='^' + NOTIFY + '$'),],
            states={
                TYPING: [MessageHandler(Filters.text & ~Filters.command, self._validate_msg),],
                SAVE: [CallbackQueryHandler(self._confirm_send_msg, pattern='^' + YES + '$|^' + NO + '$')],
            },
            fallbacks=[
                CommandHandler(cmd_stop, self._stop_conversation),
                CallbackQueryHandler(self._stop_conversation, pattern='^' + END + '$')],
            map_to_parent={
                END: MENU
            }
        )

        roll_conv = ConversationHandler(
            entry_points=[CallbackQueryHandler(self._confirm_roll, pattern='^' + START_ROLL + '$'),],
            states={
                SAVE: [CallbackQueryHandler(self._start_roll, pattern='^' + YES + '$|^' + NO + '$')],
            },
            fallbacks=[
                CommandHandler(cmd_stop, self._stop_conversation),
                CallbackQueryHandler(self._stop_conversation, pattern='^' + END + '$')],
            map_to_parent={
                END: MENU
            }
        )

        edit_wishlist_conv = ConversationHandler(
            entry_points=[CallbackQueryHandler(self._edit_wishlist, pattern='^' + EDIT_WISHLIT + '$')],
            states={
                SELECTING_ACTIONS: [
                    CallbackQueryHandler(self._start_add_gift, pattern='^' + YES + '$'),
                    CallbackQueryHandler(self._start_remove_gift, pattern='^' + NO + '$'),
                    CallbackQueryHandler(self._start_menu, pattern='^' + END + '$')
                ],
                GIFT_ADD: [
                    CallbackQueryHandler(self._typing, pattern='^' + GIFT_TITLE + '$|^' + GIFT_LINK + '$|^' + GIFT_DESCR + '$'),
                    CallbackQueryHandler(self._validate_gift_params, pattern='^' + SAVE + '$'),
                    CallbackQueryHandler(self._edit_wishlist, pattern='^' + END + '$')
                ],
                TYPING: [
                    CallbackQueryHandler(self._start_add_gift, pattern='^' + END + '$'),
                    MessageHandler(Filters.text & ~Filters.command, self._save_input)
                ],
                SAVE: [CallbackQueryHandler(self._save_gift, pattern='^' + YES + '$|^' + NO + '$')],
                GIFT_REMOVE: [
                    CallbackQueryHandler(self._remove_gift, pattern='^(?!' + END + ').*$'),
                    CallbackQueryHandler(self._edit_wishlist, pattern='^' + END + '$')
                ],
            },
            fallbacks=[
                    CommandHandler(cmd_stop, self._stop_conversation)
            ],
            map_to_parent={
                END: MENU,
                MENU: MENU
            }
        )

        main_conv = ConversationHandler(
            entry_points=[CallbackQueryHandler(self._start_conversation, pattern='^' + entry_state + '$')],
            states={
                MENU: [
                    notify_conv,
                    roll_conv,
                    edit_wishlist_conv,
                    CallbackQueryHandler(self._gift_list, pattern='^' + GIFT_LIST + '$'),
                    CallbackQueryHandler(self._give_list, pattern='^' + GIVELIST + '$'),
                    CallbackQueryHandler(self._room_members, pattern='^' + MEMBER_LIST + '$'),
                    CallbackQueryHandler(self._view_target_member, pattern='^' + TARGET_MEMBER + '$'),
                    CallbackQueryHandler(self._help, pattern='^' + HELP + '$'),
                    CallbackQueryHandler(self._room_token, pattern='^' + ROOM_TOKEN + '$'),
                    CallbackQueryHandler(self._take_gift, pattern='^' + GIFT_TAKE + '$'),
                    CallbackQueryHandler(self._drop_gift, pattern='^' + GIFT_DROP + '$'),
                    CallbackQueryHandler(self._return, pattern='^' + END + '$')
                ],
                GIFT_LIST: [CallbackQueryHandler(self._start_menu, pattern='^' + END + '$'), ],
                GIVELIST: [CallbackQueryHandler(self._start_menu, pattern='^' + END + '$'), ],
                HELP: [CallbackQueryHandler(self._start_menu, pattern='^' + END + '$'), ],
                ROOM_TOKEN: [CallbackQueryHandler(self._start_menu, pattern='^' + END + '$'), ],
                SELECT: [
                    CallbackQueryHandler(self._gift_select, pattern='^(?!' + END + ').*$'),
                    CallbackQueryHandler(self._start_menu, pattern='^' + END + '$')
                ],
                SHOWING: [CallbackQueryHandler(self._start_menu, pattern='^' + END + '$')]
            },
            fallbacks=[
                CommandHandler(cmd_stop, self._stop_conversation)
            ],
            map_to_parent={END: back_state}
        )

        return main_conv

    def _start_conversation(self, update: Update, context: CallbackContext) -> str:
        self.user_presenter.validate_user(tg_id=update.effective_user.id,
                                          chat_id=update.effective_chat.id,
                                          name=update.effective_user.full_name,
                                          link=update.effective_user.link,
                                          link_name=update.effective_user.name)

        return self._start_menu(update, context)

    @staticmethod
    def _stop_conversation(update: Update, context: CallbackContext) -> int:
        """ Stop conversation """
        context.user_data[START_OVER] = False
        update.callback_query.answer()
        update.callback_query.edit_message_text(text='До скорого!👋')
        return ConversationHandler.END

    def _return(self, update: Update, context: CallbackContext) -> int:
        """ Stop conversation """
        context.user_data[DELETE_MSG] = None
        if self.back_func:
            self.back_func(update, context)
        return END

    def _start_menu(self, update: Update, context: CallbackContext, text: str = '') -> str:
        """ Main menu """
        room = self.__get_active_room(update.effective_user.id)
        buttons = []
        if room:
            text += '👥 Активная комната:\nНазвание: {0}\nДата события: {1}'.format(room.title, room.date.date())
            buttons.append([InlineKeyboardButton(text='📋 Список участников', callback_data=MEMBER_LIST)])
            if room.regime == 'Тайный санта':
                buttons.append([InlineKeyboardButton(text='❔ Кому я дарю?', callback_data=TARGET_MEMBER)])
                if room.owner_id == update.effective_user.id:
                    buttons.append([InlineKeyboardButton(text='▶️ Запустить распределение', callback_data=START_ROLL)])
                    buttons.append([InlineKeyboardButton(text='📣 Отправить оповещение', callback_data=NOTIFY)])
                    buttons.append([InlineKeyboardButton(text='🔑 Ключ для приглашения', callback_data=ROOM_TOKEN)])
            elif room.regime == 'День рождения':
                buttons.append([InlineKeyboardButton(text='🎁 Список подарков', callback_data=GIFT_LIST)])
                if room.owner_id == update.effective_user.id:
                    buttons.append([InlineKeyboardButton(text='🖊 Редактировать список подарков', callback_data=EDIT_WISHLIT)])
                    buttons.append([InlineKeyboardButton(text='📣 Отправить оповещение', callback_data=NOTIFY)])
                    buttons.append([InlineKeyboardButton(text='🔑 Ключ для приглашения', callback_data=ROOM_TOKEN)])
                else:
                    buttons.append([InlineKeyboardButton(text='❔ Выбранные подарки', callback_data=GIVELIST)])
                    buttons.append([InlineKeyboardButton(text='Занять подарок', callback_data=GIFT_TAKE)])
                    buttons.append([InlineKeyboardButton(text='Освободить подарок', callback_data=GIFT_DROP)])
            else:
                raise 'Неизвестный режим'
        else:
            # TODO Здесь надо показать кнопки на смену комнату и вход по токену
            text += '❗️ У вас нет активной комнаты. Вступите в какую-то комнату или выберете из вашего списка.' \
                    'Это можно сделать в меню смены комнаты.'

        buttons.append([InlineKeyboardButton(text='❓ Помощь', callback_data=HELP)])
        buttons.append([InlineKeyboardButton(text='⬅️ Назад', callback_data=END)])
        keyboard = InlineKeyboardMarkup(buttons)

        if context.user_data.get(START_OVER):
            update.callback_query.answer()
            update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        else:
            context.user_data[START_OVER] = True
            update.message.reply_text(text=text, reply_markup=keyboard)

        return MENU

    @staticmethod
    def _help(update: Update, context: CallbackContext) -> str:
        """ Get help info about room menu """
        text = ('Вы в меню взаимодействия с активной комантой, в зависимости от режима в меню могут присутствовать те '
                'или иные кнопки.\n'
                'Постоянные кнопки:\n"📋 Список участников" - вывести список участников активной комнаты.\n'
                '"❓ Помощь" - показать подсказку и "⬅️ Назад" - вернуться в предыдущее меню.\n'
                'Вариативные кнопки:\n'
                'Режим "🎅 Тайный санта" - добавляет 3 кнопки если вы владелец комнаты и 1 если вы не владелец:\n'
                '"❔ Кому я дарю?" - показывает участника которому вы будете дарить подарок.\n'
                '"▶️ Запустить распределение" (владелец) - назначает каждому участнику цель для подарка, !!! если '
                'после распределения изменились участники то его необходимо запустить заново!\n'
                '"📣 Отправить оповещение"(владелец) - отправляет сообщение всем участникам комнаты.\n'
                'Режим "🎉 День рождения" - в разработке.')
        button = InlineKeyboardButton(text='⬅️ Назад', callback_data=END)
        keyboard = InlineKeyboardMarkup.from_button(button)

        update.callback_query.answer()
        update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        return HELP

    def _room_token(self, update: Update, context: CallbackContext) -> str:
        room = self.__get_active_room(update.effective_user.id)
        button = InlineKeyboardButton(text='⬅️ Назад', callback_data=END)
        keyboard = InlineKeyboardMarkup.from_button(button)

        update.callback_query.answer()
        update.callback_query.edit_message_text(text=room.token, reply_markup=keyboard)
        return ROOM_TOKEN

    def _room_members(self, update: Update, context: CallbackContext) -> str:
        room = self.__get_active_room(update.effective_user.id)
        if room:
            text = '📋 Участники комнаты {0}\n{1}\n'.format(room.title, '-' * 50)
            for i, avatar in enumerate(room.list):
                user = self.user_presenter.user(avatar.tg_id)
                if user:
                    text += '{0}. {1} - {2}\n'.format(i, user.name, user.link_name)
        else:
            text = '❗️ У вас нет активной комнаты, пожалуйста вступите в каку-то комнату или выберете из вашего списка.' \
                   'Это можно сделать в щелкнув на /room или выбрав соответствующий пункт в меню слева от поля ввода.'

        button = InlineKeyboardButton(text='⬅️ Назад', callback_data=END)
        keyboard = InlineKeyboardMarkup.from_button(button)

        update.callback_query.answer()
        update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        return SHOWING

    @staticmethod
    def _ask_input_msg(update: Update, context: CallbackContext) -> str:
        """ Save notify message """
        update.callback_query.answer()
        update.callback_query.edit_message_text(text='🖊 Окей, скажи мне что отправить участникам комнаты?')
        return TYPING

    @staticmethod
    def _validate_msg( update: Update, context: CallbackContext) -> str:
        """ Ask confirm send message """
        context.user_data[MESSAGE] = update.message.text
        text = '📬 Отправить сообщение "{0}" всем участникам данной комнаты?'.format(update.message.text)
        buttons = [
            [
                InlineKeyboardButton(text='✅ Да', callback_data=YES),
                InlineKeyboardButton(text='❌ Нет', callback_data=NO)
            ]
        ]
        keyboard = InlineKeyboardMarkup(buttons)
        update.message.reply_text(text=text, reply_markup=keyboard)
        return SAVE

    def _confirm_send_msg(self, update: Update, context: CallbackContext) -> str:
        """ Check choose and send message all room members """
        if update.callback_query.data == YES:
            room = self.__get_active_room(update.effective_user.id)
            if room:
                msg = '✉️ Оповещение из комнаты "{0}":\n{1}'.format(room.title, context.user_data[MESSAGE])
                for avatar in room.list:
                    if avatar.tg_id != update.effective_user.id:
                        user = self.user_presenter.user(avatar.tg_id)
                        self.__send_message(user, msg)
                text = '✅ Сообщение доставлено всем кому удалось!\n'
            else:
                text = '❗️ Похоже у вас нет активной комнаты.\n'
        else:
            text = '✅ Хорошо, пока не отправляю.\n'

        self._start_menu(update, context, text)
        return END

    def _view_target_member(self, update: Update, context: CallbackContext) -> str:
        """ View target member in hidden santa mode """
        user = self.user_presenter.user(update.effective_user.id)
        text = '🤭 Ошибка, свяжитесь с разработчиком!\n'
        if user and user.active_avatar:
            avatar = user.active_avatar
            if avatar.target_avatar_id:
                target_avatar = self.avatar_presenter.avatar(avatar.target_avatar_id)
                target_user = self.user_presenter.user(target_avatar.tg_id)
                text = '🧍 Назначеный пользователь в текущей комнате: {0} - {1}\n'.format(target_user.name,
                                                                                       target_user.link_name)
            else:
                text = '❗️ Распределение еще не запусказлось или было сброшено.\nУ вас нет назначенного пользователя!\n'

        return self._start_menu(update, context, text)

    @staticmethod
    def _confirm_roll(update: Update, context: CallbackContext) -> str:
        text = 'Запустить распределение? ⚠️ Если распределение уже запускалось пользователи будут переназначены!'
        buttons = [
            [
                InlineKeyboardButton(text='✅ Да', callback_data=YES),
                InlineKeyboardButton(text='❌ Нет', callback_data=NO)
            ]
        ]

        keyboard = InlineKeyboardMarkup(buttons)
        update.callback_query.answer()
        update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        return SAVE

    def _start_roll(self, update: Update, context: CallbackContext) -> str:
        """ Start member roll in hidden santa mode"""
        if update.callback_query.data == YES:
            user = self.user_presenter.user(update.effective_user.id)
            if user and user.active_room_id:
                room = self.room_presenter.room(user.active_room_id)
                random.shuffle(room.list)
                avatars = room.list

                msg_template = '🎉 Завершилось распределение участников в комнате "' + room.title + '".\n' \
                               'Вы дарите подарок пользователю {0} - {1}\n'
                def send_msg(self_id: int, target_id: int):
                    self_user = self.user_presenter.user(self_id)
                    target_user = self.user_presenter.user(target_id)
                    self.__send_message(self_user, msg_template.format(target_user.name, target_user.link_name))

                for i in range(0, len(avatars) - 1):
                    self.avatar_presenter.set_target_avatar(avatars[i].id, avatars[i + 1].id)
                    send_msg(avatars[i].tg_id, avatars[i + 1].tg_id)
                self.avatar_presenter.set_target_avatar(avatars[len(avatars) - 1].id, avatars[0].id)
                send_msg(avatars[len(avatars) - 1].tg_id, avatars[0].tg_id)

                text = '🎉 Распределение завершено. Всем участникам отправлено уведомление!\n'
            else:
                text = '🤭 Ошибка, свяжитесь с разработчиком!\n'
        else:
            text = '✅ Хорошо, пока не зпускаю распределение!\n'

        self._start_menu(update, context, text)
        return END

    def _gift_list(self, update: Update, context: CallbackContext) -> str:
        room = self.__get_active_room(update.effective_user.id)

        avatar = None
        for av in room.list:
            if av.tg_id == room.owner_id:
                avatar = av
                break

        if avatar:
            gifts = self.gift_presenter.gifts(avatar.wish_list_ids)
            if room.owner_id == update.effective_user.id:
                if gifts:
                    text = 'Список ваших подарков:\n'
                    for i, gift in enumerate(gifts):
                        text += '{0}. {1} - {2} - {3}\n'.format(i, gift.title, gift.link, gift.description)
                else:
                    text = 'Список ваших подарков пуст!'
            else:
                user = self.user_presenter.user(room.owner_id)
                text = '🔴 - Занят. 🟢 - Свободен\n'
                if gifts:
                    text += 'Список подарков пользователя {0}:\n'.format(user.name)
                else:
                    text += 'Список подарков пользователя {0} пуст!'.format(user.name)

                text += '-' * 35 + '\n'
                for i, gift in enumerate(gifts):
                    text += '{0} {1}. {2} - {3} - {4}\n'.format('🔴' if gift.giver_ids else '🟢',
                                                                i, gift.title, gift.link, gift.description)

            button = InlineKeyboardButton(text='⬅️ Назад', callback_data=END)
            keyboard = InlineKeyboardMarkup.from_button(button)

            update.callback_query.answer()
            update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
            return GIFT_LIST

        update.callback_query.answer()
        return self._start_menu(update, context, 'Неизвестная ошибка')

    def _give_list(self, update: Update, context: CallbackContext) -> str:
        room = self.__get_active_room(update.effective_user.id)
        avatar = self.user_presenter.user(update.effective_user.id).active_avatar

        text = 'Комната "{0}". Дата события: {1}\n'.format(room.title, room.date.date())
        text += 'Список подарков, которые вы дарите:\n' + '-' * 35 + '\n'
        gifts = self.gift_presenter.gifts(avatar.give_list_ids)
        for i, gift in enumerate(gifts):
            text += "{0}. {1} - {2} - {3}\n".format(i, gift.title, gift.link, gift.description)

        button = InlineKeyboardButton(text='⬅️ Назад', callback_data=END)
        keyboard = InlineKeyboardMarkup.from_button(button)

        update.callback_query.answer()
        update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        return GIVELIST

    @staticmethod
    def _edit_wishlist(update: Update, context: CallbackContext) -> str:
        context.user_data[GIFT_TITLE] = ''
        context.user_data[GIFT_LINK] = ''
        context.user_data[GIFT_DESCR] = ''

        text = 'Что вы хотите сделать с вашим списком подарков?'
        buttons = [
            [
                InlineKeyboardButton(text='❇️ Добавить подарок', callback_data=YES),
                InlineKeyboardButton(text='❌ Удалить подарок', callback_data=NO)
            ],
            [InlineKeyboardButton(text='⬅️ Назад', callback_data=END)]
        ]

        keyboard = InlineKeyboardMarkup(buttons)
        update.callback_query.answer()
        update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        return SELECTING_ACTIONS

    @staticmethod
    def _start_add_gift(update: Update, context: CallbackContext, text: str = '') -> str:
        text += 'Обязательное поле для заполнения - Название. Ссылка и комментарий - по желанию!\n' \
                'После того как все заполнено - нажмите добавить.'

        buttons = [
            [
                InlineKeyboardButton(text='Название', callback_data=GIFT_TITLE),
                InlineKeyboardButton(text='Ссылка', callback_data=GIFT_LINK),
                InlineKeyboardButton(text='Комментарий', callback_data=GIFT_DESCR)
            ],
            [InlineKeyboardButton(text='🆗 Добавить', callback_data=SAVE)],
            [InlineKeyboardButton(text='⬅️ Назад', callback_data=END)]
        ]

        keyboard = InlineKeyboardMarkup(buttons)
        if context.user_data[START_OVER]:
            update.callback_query.answer()
            update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        else:
            context.user_data[START_OVER] = True
            update.message.reply_text(text=text, reply_markup=keyboard)
        return GIFT_ADD

    @staticmethod
    def _typing(update: Update, context: CallbackContext) -> str:
        names = {GIFT_TITLE: 'название', GIFT_LINK: 'ссылку', GIFT_DESCR: 'комментарий'}
        field = update.callback_query.data
        text = 'Окей, отправте мне {0}'.format(names[field])
        context.user_data[TYPING] = field
        button = InlineKeyboardButton(text='⬅️ Назад', callback_data=END)
        keyboard = InlineKeyboardMarkup.from_button(button)
        update.callback_query.answer()
        message = update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        context.user_data[DELETE_MSG] = message.message_id
        return TYPING

    def _save_input(self, update: Update, context: CallbackContext):
        text = 'Это запомнил\n'
        context.user_data[context.user_data[TYPING]] = update.message.text
        context.user_data[START_OVER] = False
        self.bot.delete_message(update.message.chat_id, context.user_data[DELETE_MSG])
        return self._start_add_gift(update, context, text)

    def _validate_gift_params(self, update: Update, context: CallbackContext) -> str:
        title = context.user_data[GIFT_TITLE]
        if not title:
            text = 'Необходимо указать название подарка!'
            return self._start_add_gift(update, context, text)
        else:
            link = context.user_data[GIFT_LINK] if GIFT_LINK in context.user_data else ''
            descr = context.user_data[GIFT_DESCR] if GIFT_DESCR in context.user_data else ''
            text = 'Добавить подарок:\nНазвание: {0}\nСсылка: {1}\nОписание: {2}'.format(title, link, descr)
            buttons = [
                [
                    InlineKeyboardButton(text='✅ Да', callback_data=YES),
                    InlineKeyboardButton(text='❌ Нет', callback_data=NO)
                ]
            ]
            keyboard = InlineKeyboardMarkup(buttons)
            update.callback_query.answer()
            update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
            return SAVE

    def _save_gift(self, update: Update, context: CallbackContext) -> str:
        if update.callback_query.data == YES:
            title = context.user_data[GIFT_TITLE]
            link = context.user_data[GIFT_LINK] if GIFT_LINK in context.user_data else ''
            descr = context.user_data[GIFT_DESCR] if GIFT_DESCR in context.user_data else ''
            user = self.user_presenter.user(update.effective_user.id)
            if self.gift_presenter.add_gift(user.active_avatar.id, title, link, descr):
                text = '✅ Подарок "{0}" успешно добавлен!\n'.format(title)
            else:
                text = '🤭 Ошибка добавления подарка, пожалуйста, повторите попытку.\n'
            context.user_data[GIFT_TITLE] = ''
            context.user_data[GIFT_LINK] = ''
            context.user_data[GIFT_DESCR] = ''
        else:
            text = '👌🏻 Хорошо, пока не добавляю подарок!\n'

        return self._start_add_gift(update, context, text)

    def _start_remove_gift(self, update: Update, context: CallbackContext, text: str = '') -> str:
        user = self.user_presenter.user(update.effective_user.id)
        gifts = self.gift_presenter.gifts(user.active_avatar.wish_list_ids)
        buttons = []
        for gift in gifts:
            buttons.append([InlineKeyboardButton(text=gift.title, callback_data=str(gift.id))])
        buttons.append([InlineKeyboardButton(text='⬅️ Назад', callback_data=END)])
        keyboard = InlineKeyboardMarkup(buttons)
        text += 'Ваш список подарков сейчас пуст!' if len(buttons) == 1 else 'Какой подарок вы хотите удалить?'
        update.callback_query.answer()
        update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        return GIFT_REMOVE

    def _remove_gift(self, update: Update, context: CallbackContext) -> str:
        removed_id = int(update.callback_query.data)
        if self.gift_presenter.remove_gift(removed_id):
            text = 'Подарок успешно удален!\n'
        else:
            text = 'Не удалось удалить подарок, попробуйте еще раз!\n'

        return self._start_remove_gift(update, context, text)

    def _take_gift(self, update: Update, context: CallbackContext, text: str = '') -> str:
        room = self.__get_active_room(update.effective_user.id)
        owner_avatar = self.avatar_presenter.avatar_from_room(room.owner_id, room.id)
        gifts = self.gift_presenter.gifts(owner_avatar.wish_list_ids)
        buttons = []
        for gift in gifts:
            if not gift.giver_ids:
                buttons.append([InlineKeyboardButton(text=gift.title, callback_data=str(gift.id))])
        buttons.append([InlineKeyboardButton(text='⬅️ Назад', callback_data=END)])
        keyboard = InlineKeyboardMarkup(buttons)
        if len(buttons) == 1:
            text += 'Вишлист еще не составлен или все занято 🤔'
        else:
            text += 'Выберете подарок, который будете дарить:'
        context.user_data[SELECT] = GIFT_TAKE
        update.callback_query.answer()
        update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        return SELECT

    def _drop_gift(self, update: Update, context: CallbackContext, text: str = '') -> str:
        effective_avatar = self.user_presenter.user(update.effective_user.id).active_avatar
        gifts = self.gift_presenter.gifts(effective_avatar.give_list_ids)
        buttons = []
        for gift in gifts:
            buttons.append([InlineKeyboardButton(text=gift.title, callback_data=str(gift.id))])
        buttons.append([InlineKeyboardButton(text='⬅️ Назад', callback_data=END)])
        keyboard = InlineKeyboardMarkup(buttons)
        text += 'Вы не выбрали ни одного подарка' if len(buttons) == 1 else 'Выберете подарок, который не хотите дарить:'
        context.user_data[SELECT] = GIFT_DROP
        update.callback_query.answer()
        update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        return SELECT

    def _gift_select(self, update: Update, context: CallbackContext):
        act_type = context.user_data[SELECT]
        act_id = int(update.callback_query.data)
        if act_type == GIFT_TAKE or act_type == GIFT_DROP:
            effective_avatar = self.user_presenter.user(update.effective_user.id).active_avatar
            if act_type == GIFT_TAKE:
                self.gift_presenter.add_giver(act_id, effective_avatar.id)
            else:
                self.gift_presenter.remove_giver(act_id, effective_avatar.id)
            text = 'Успешно!\n'
        else:
            text = '🤭 Ошибка, свяжитесь с разработчиком!\n'

        return self._take_gift(update, context, text) if act_type == GIFT_TAKE else self._drop_gift(update, context, text)

    def __get_active_room(self, tg_id: int) -> Room or None:
        """ Get active room by telegram id """
        user = self.user_presenter.user(tg_id)
        if user and user.active_room_id:
            return self.room_presenter.room(user.active_room_id)
        else:
            return None

    def __send_message(self, user: User, msg: str) -> bool:
        try:
            self.bot.send_message(user.chat_id, msg)
            return True
        except TelegramError:
            # TODO add log?
            return False