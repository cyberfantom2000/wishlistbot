from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update, Bot
from telegram.ext import CallbackContext, CommandHandler, CallbackQueryHandler, MessageHandler, Filters
from handlers.defines import *
from presenters.userpresenter import UserPresenter
from presenters.roompresenter import RoomPresenter
from presenters.avatarpresenter import AvatarPresenter


class ChangeRoomMenu:
    def __init__(self, user_presenter: UserPresenter,
                 room_presenter: RoomPresenter,
                 avatar_presenter: AvatarPresenter,
                 bot: Bot):
        self.user_presenter = user_presenter
        self.room_presenter = room_presenter
        self.avatar_presenter = avatar_presenter
        self.bot = bot
        self.back_func = None

    def get_conversation_handler(self, entry_state: str, cmd_stop: str, back_func,
                                 back_state: str) -> ConversationHandler:
        self.back_func = back_func

        handler = ConversationHandler(
            entry_points=[CallbackQueryHandler(self._start_conversation, pattern='^' + entry_state + '$')],
            states={
                MENU: [
                    CallbackQueryHandler(self._start_change_room, pattern='^' + ROOM_CHANGE + '$'),
                    CallbackQueryHandler(self._start_insert_to_room, pattern='^' + ROOM_ENTER + '$'),
                    CallbackQueryHandler(self._room_help, pattern='^' + HELP + '$'),
                    CallbackQueryHandler(self._return, pattern='^' + END + '$')
                ],
                SELECT: [
                    CallbackQueryHandler(self._change_room, pattern='^(?!' + END + ').*$'),
                    CallbackQueryHandler(self._start_menu, pattern='^' + END + '$')
                ],
                TYPING: [
                    MessageHandler(Filters.text & ~Filters.command, self._save_input_token),
                    CallbackQueryHandler(self._start_menu, pattern='^' + END + '$')
                ],
                HELP: [CallbackQueryHandler(self._start_menu, pattern='^' + END + '$'), ],
            },
            fallbacks=[
                CommandHandler(cmd_stop, self._stop_menu)
            ],
            map_to_parent={END: back_state}
        )

        return handler

    def _start_conversation(self, update: Update, context: CallbackContext) -> str:
        """ Start room control conversation """
        self.user_presenter.validate_user(tg_id=update.effective_user.id,
                                          chat_id=update.effective_chat.id,
                                          name=update.effective_user.full_name,
                                          link=update.effective_user.link,
                                          link_name=update.effective_user.name)

        return self._start_menu(update, context)

    @staticmethod
    def _start_menu(update: Update, context: CallbackContext, text: str = '') -> str:
        """ Main room ctrl menu """
        buttons = [
            [InlineKeyboardButton(text='🔁 Сменить комнату', callback_data=ROOM_CHANGE)],
            [InlineKeyboardButton(text='🔑 Войти по ключу', callback_data=ROOM_ENTER)],
            [InlineKeyboardButton(text='❓ Помощь', callback_data=HELP)],
            [InlineKeyboardButton(text='⬅️ Назад', callback_data=END)]
        ]
        keyboard = InlineKeyboardMarkup(buttons)
        msg = '🔁 Здесь вы можете вступить в новую комнату или выбрать из списка доступных.\n' + text

        if context.user_data.get(START_OVER):
            update.callback_query.answer()
            update.callback_query.edit_message_text(text=msg, reply_markup=keyboard)
        else:
            context.user_data[START_OVER] = True
            update.message.reply_text(text=msg, reply_markup=keyboard)
        return MENU

    @staticmethod
    def _stop_menu(update: Update, context: CallbackContext) -> int:
        """ Stop conversation """
        context.user_data[START_OVER] = False
        context.user_data[DELETE_MSG] = None
        update.callback_query.answer()
        update.callback_query.edit_message_text(text='До скорого!')
        return ConversationHandler.END

    def _return(self, update: Update, context: CallbackContext) -> str:
        context.user_data[DELETE_MSG] = None
        if self.back_func:
            self.back_func(update, context)
        return END

    def _start_change_room(self, update: Update, context: CallbackContext) -> str:
        """ Start change room conversation """
        user = self.user_presenter.user(update.effective_user.id)
        buttons = []
        if not user:
            text = '🫢 Ошибка, свяжитесь с разработчиком!'
        else:
            for room_id in user.room_ids:
                room = self.room_presenter.room(room_id)
                if room:
                    btn_text = '{0} (Дата события: {1})'.format(room.title, room.date.date())
                    buttons.append([InlineKeyboardButton(text=btn_text, callback_data=str(room.id))])

            if buttons:
                text = '📋 Список доступных комнат. В какую команту перейти?'
            else:
                text = '❗️ У вас нет доступных комнат, создайте новую комнату или войдите по пригласительному ключу.' \
                       'После этого комната появится в этом списке.'

        buttons.append([InlineKeyboardButton(text='⬅️ Назад', callback_data=END)])
        keyboard = InlineKeyboardMarkup(buttons)

        update.callback_query.answer()
        update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        return SELECT

    def _change_room(self, update: Update, context: CallbackContext) -> str:
        """ Try to change room """
        new_room_id = int(update.callback_query.data)
        if self.user_presenter.change_active_room(update.effective_user.id, new_room_id):
            text = '✅ Активная комната успешно изменена!\n'
        else:
            text = '❗️ Ошибка смены комнаты!\n'

        return self._start_menu(update, context, text)

    @staticmethod
    def _start_insert_to_room(update: Update, context: CallbackContext) -> str:
        """ Start insert to room conversation """
        text = '🖊 Введите токен для входа в комнату'
        button = InlineKeyboardButton(text='⬅️ Назад', callback_data=END)
        keyboard = InlineKeyboardMarkup.from_button(button)
        update.callback_query.answer()
        message = update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        context.user_data[DELETE_MSG] = message.message_id
        return TYPING

    def _save_input_token(self, update: Update, context: CallbackContext) -> str:
        """ Take token from message and try to enter the room """
        tg_id = update.effective_user.id
        token = update.message.text.strip()
        found_room = self.user_presenter.enter_to_room(tg_id, token)
        if found_room:
            if (self.avatar_presenter.create_avatar(tg_id, found_room.id) and
                    self.user_presenter.change_active_room(tg_id, found_room.id)):
                text = '✅ Вы успешно зарегестрировались в комнате "{0}". Эта комната выбрана активной!\n'
            else:
                text = '✅ Вы успешно зарегестрировались в комнате "{0}", ❗️ но она не выбрана активной\n' \
                       'Для взаимодействия с комнатой попробуйте выбрать ее через пунтк "Сменить комнату".'

            text = text.format(found_room.title)
            context.user_data[START_OVER] = False
            self.bot.delete_message(update.message.chat_id, context.user_data[DELETE_MSG])
            self._start_menu(update, context, text)
            return END
        else:
            text = '❗️ Комната по токену {} не найдена. Попробуйте другой токен'.format(token)
            button = InlineKeyboardButton(text='⬅️ Назад', callback_data=END)
            keyboard = InlineKeyboardMarkup.from_button(button)
            self.bot.delete_message(update.message.chat_id, context.user_data[DELETE_MSG])
            message = update.message.reply_text(text=text, reply_markup=keyboard)
            context.user_data[DELETE_MSG] = message.message_id
            return TYPING

    @staticmethod
    def _room_help(update: Update, context: CallbackContext) -> str:
        """ Get help info about change room menu """
        text = ('Вы в меню смены комнаты. Здесь вы можете изменить активную комнату или вступить в комнату, используя'
                'пригласительный ключ.\n'
                'Более подробно по каждому пункту:\n'
                'Кнопка "🔁 Сменить комнату" позволяет сменить активную комнату на любую в которой вы уже состоите.\n'
                'Кнопка "🔑 Войти по ключу" позволяет ввести токен приглашения, чтобы вступить в комнату.\n'
                'Кнопка "❓ Помощь" - откроет подсказку.\n'
                'Кнопка "⬅️ Назад" - вернуться в предыдущее меню.')
        button = InlineKeyboardButton(text='⬅️ Назад', callback_data=END)
        keyboard = InlineKeyboardMarkup.from_button(button)

        update.callback_query.answer()
        update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        return HELP
