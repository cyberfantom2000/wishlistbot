from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update, Bot
from telegram.ext import (CallbackContext,
                          CommandHandler,
                          MessageHandler,
                          Filters,
                          CallbackQueryHandler)
from handlers.defines import *
from datetime import datetime
from presenters.roompresenter import RoomPresenter
from presenters.userpresenter import UserPresenter
from presenters.avatarpresenter import AvatarPresenter


class CreateRoomMenu:
    def __init__(self, user_presenter: UserPresenter,
                 room_presenter: RoomPresenter,
                 avatar_presenter: AvatarPresenter,
                 bot: Bot) -> None:
        self.room_presenter = room_presenter
        self.user_presenter = user_presenter
        self.avatar_presenter = avatar_presenter
        self.bot = bot
        self.back_func = None

    def get_conversation_handler(self, entry_state: str, cmd_stop: str, back_func, back_state: str) -> ConversationHandler:
        self.back_func = back_func

        handler = ConversationHandler(
            entry_points=[CallbackQueryHandler(self._start_conversation, pattern='^' + entry_state + '$')],
            states={
                MENU: [
                    CallbackQueryHandler(self._ask_input_room, pattern='^(?!' + END + ').*$'),
                    CallbackQueryHandler(self._return, pattern='^' + END + '$')
                ],
                TYPING: [
                    CallbackQueryHandler(self._start_menu, pattern='^' + END + '$'),
                    MessageHandler(Filters.text & ~Filters.command, self._save_input_room)
                ],
                SELECT: [
                    CallbackQueryHandler(self._start_menu, pattern='^' + END + '$'),
                    CallbackQueryHandler(self._save_input_room, pattern='^(?!' + END + ').*$')
                ],
                SAVE: [CallbackQueryHandler(self._confirm_save_room, pattern='^' + YES + '$|^' + NO + '$')],
                HELP: [CallbackQueryHandler(self._start_menu, pattern='^' + END + '$')]
            },
            fallbacks=[
                CommandHandler(cmd_stop, self._stop_menu),
                CallbackQueryHandler(self._stop_menu, pattern='^' + END + '$')
            ],
            map_to_parent={END: back_state}
        )
        return handler

    def _start_conversation(self, update: Update, context: CallbackContext) -> str:
        """ Start create room conversation """
        self.user_presenter.validate_user(tg_id=update.effective_user.id,
                                          chat_id=update.effective_chat.id,
                                          name=update.effective_user.full_name,
                                          link=update.effective_user.link,
                                          link_name=update.effective_user.name)
        context.user_data[ROOM_NAME] = ''
        context.user_data[ROOM_REGIME] = ''
        context.user_data[ROOM_DATE] = ''
        return self._start_menu(update, context)

    def _restart_conversation(self, update: Update, context: CallbackContext) -> str:
        return self._start_conversation(update, context)

    @staticmethod
    def _start_menu(update: Update, context: CallbackContext, msg: str = '') -> str:
        """ Start create new room """
        if msg:
            text = msg
        else:
            text = ('🆕 Для новой комнаты потребуется выбрать имя, режим и дату события. Когда все поля '
                    'будут заполнены - нажмите кнопку "Создать"!\n'
                    'Заполнять можно в любом порядке. Для изменения поля - перезаполните его заново.')

        buttons = [
            [
                InlineKeyboardButton(text='🔤 Название', callback_data=ROOM_NAME),
                InlineKeyboardButton(text='🔣 Режим', callback_data=ROOM_REGIME),
                InlineKeyboardButton(text='🕒 Дата события', callback_data=ROOM_DATE)
            ],
            [InlineKeyboardButton(text='🆗 Создать', callback_data=SAVE)],
            [InlineKeyboardButton(text='⬅️ Назад', callback_data=END)]
        ]

        keyboard = InlineKeyboardMarkup(buttons)

        if context.user_data.get(START_OVER):
            update.callback_query.answer()
            update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        else:
            context.user_data[START_OVER] = True
            if update.message:
                update.message.reply_text(text=text, reply_markup=keyboard)
            else:
                update.callback_query.message.reply_text(text=text, reply_markup=keyboard)
        return MENU

    @staticmethod
    def _stop_menu(update: Update, context: CallbackContext) -> int:
        context.user_data[DELETE_MSG] = None
        context.user_data[ROOM_NAME] = ''
        context.user_data[ROOM_REGIME] = ''
        context.user_data[ROOM_DATE] = ''
        update.callback_query.answer()
        update.callback_query.edit_message_text(text='До скорого!')
        return ConversationHandler.END

    def _return(self, update: Update, context: CallbackContext) -> str:
        context.user_data[DELETE_MSG] = None
        context.user_data[ROOM_NAME] = ''
        context.user_data[ROOM_REGIME] = ''
        context.user_data[ROOM_DATE] = ''
        if self.back_func:
            self.back_func(update, context)
        return END

    def _ask_input_room(self, update: Update, context: CallbackContext) -> str:
        """ Prompt user to input title new room """
        feature = update.callback_query.data
        context.user_data[STATE] = feature
        if feature == ROOM_REGIME:
            text = 'Выберите режим комнаты'
            buttons = [
                [InlineKeyboardButton(text='🎅 Тайный санта', callback_data=REGIME_SANTA)],
                [InlineKeyboardButton(text='🎁 День рождения', callback_data=REGIME_BIRTHDAY)],
                [InlineKeyboardButton(text='⬅️ Назад', callback_data=END)]
            ]
            keyboard = InlineKeyboardMarkup(buttons)
            update.callback_query.answer()
            update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
            return SELECT
        elif feature == ROOM_NAME or feature == ROOM_DATE:
            format_text = "название комнаты" if feature == ROOM_NAME else 'дату события в формате дд.мм.гг'
            text = 'Окей отправте мне {0}'.format(format_text)
            button = InlineKeyboardButton(text='⬅️ Назад', callback_data=END)
            keyboard = InlineKeyboardMarkup.from_button(button)
            update.callback_query.answer()
            message = update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
            context.user_data[DELETE_MSG] = message.message_id
            return TYPING
        elif feature == SAVE:
            return self._validate_room_params(update, context)
        else:
            update.callback_query.answer()
            update.message.reply_text(text='🫢 Неизвестная ошибка. Попробуйте снова.')
            return END

    def _save_input_room(self, update: Update, context: CallbackContext) -> str:
        """ Save input data to user context """
        state = context.user_data.get(STATE)
        text = '✅ Запомнил! Когда все поля будут заполнены - нажмите "Создать"!'
        if state == ROOM_REGIME:
            regime_names = {REGIME_SANTA: 'Тайный санта', REGIME_BIRTHDAY: 'День рождения'}
            context.user_data[ROOM_REGIME] = regime_names[update.callback_query.data]
        elif state == ROOM_NAME:
            context.user_data[ROOM_NAME] = update.message.text
        elif state == ROOM_DATE:
            try:
                date = datetime.strptime(update.message.text.rstrip(), '%d.%m.%y')
                now = datetime.now()
                if now < date and (date - now).days < 366:
                    context.user_data[ROOM_DATE] = date.date()
                else:
                    text = ('❗️ Не корректная дата события {}!\nНельзя указывать событие в прошлом и более '
                            'чем через год!\nПопробуйте снова!').format(date.date())
            except ValueError:
                text = '❗️ Не корректная дата события!\nДопустимый формат даты: дд.мм.гг\nПопробуйте снова!'
        else:
            text = '🫢 Возникла какая-то ошибка, попробуйте снова!'

        if state == ROOM_NAME or state == ROOM_DATE:
            context.user_data[START_OVER] = False
            self.bot.delete_message(update.message.chat_id, context.user_data[DELETE_MSG])

        return self._start_menu(update, context, text)

    def _validate_room_params(self, update: Update, context: CallbackContext) -> str:
        title = context.user_data.get(ROOM_NAME)
        regime = context.user_data.get(ROOM_REGIME)
        date = context.user_data.get(ROOM_DATE)

        if not regime or not date or not title:
            text = ('❗️ Для создания комнаты все параметры должны быть заполнены!\n'
                    'Название: {0}\nРежим: {1}\nДата: {2}\n').format(title, regime, date)
            return self._start_menu(update, context, text)
        else:
            text = ('Создать комнату со следующими параметрами:\n'
                    'Название: {0}\nРежим: {1}\nДата события: {2}\n?').format(title, regime, date)
            buttons = [
                [
                    InlineKeyboardButton(text='✅ Да', callback_data=YES),
                    InlineKeyboardButton(text='❌ Нет', callback_data=NO)
                ]
            ]
            keyboard = InlineKeyboardMarkup(buttons)
            update.callback_query.answer()
            update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
            return SAVE

    def _confirm_save_room(self, update: Update, context: CallbackContext):
        feature = update.callback_query.data
        if feature == YES:
            title = context.user_data.get(ROOM_NAME)
            regime = context.user_data.get(ROOM_REGIME)
            date = context.user_data.get(ROOM_DATE)
            tg_id = update.effective_user.id

            room = self.room_presenter.create_room(tg_id, title, regime, date)
            if room:
                if (self.avatar_presenter.create_avatar(tg_id, room.id) and
                    self.user_presenter.change_active_room(tg_id, room.id)):
                    added_text = '✅ Новая комната выбрана активной!'
                else:
                    added_text = ('🫢 Произошла ошибка и комната не выбрана активной, '
                                  'смените комнату через соответствующее меню')
                text = ('😎 Комната {0} успешно создана! Она будет удалена менее чем через сутки после {1}.\n'
                        'Для приглашения участников отправте им ключ комнаты:').format(title, date)
                update.callback_query.answer()
                update.callback_query.edit_message_text(text=text)
                update.callback_query.message.reply_text(text=room.token)
                text = added_text
                context.user_data[START_OVER] = False
            else:
                text = '🫢 Ошибка создания комнаты, пожалуйста, повторите попытку.'
        else:
            text = '👌🏻 Хорошо, пока не создаю комнату!'
        return self._start_menu(update, context, text)
