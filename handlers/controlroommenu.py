from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update, Bot
from telegram.ext import CallbackContext, CommandHandler, CallbackQueryHandler
from handlers.defines import *
from presenters.userpresenter import UserPresenter
from presenters.roompresenter import RoomPresenter
from presenters.avatarpresenter import AvatarPresenter
from core.entities import Room

class ControlRoomMenu:
    def __init__(self,
                 user_presenter: UserPresenter,
                 room_presenter: RoomPresenter,
                 avatar_presenter: AvatarPresenter,
                 bot: Bot) -> None:
        self.user_presenter = user_presenter
        self.room_presenter = room_presenter
        self.avatar_presenter = avatar_presenter
        self.bot = bot
        self.back_func = None

    def get_conversation_handler(self, entry_state: str, cmd_stop: str, back_func, back_state: str) -> ConversationHandler:
        """ Create conversation handler """
        self.back_func = back_func

        handler = ConversationHandler(
            entry_points=[CallbackQueryHandler(self._start_conversation, pattern='^' + entry_state + '$')],
            states={
                MENU: [
                    # TODO: Удалить комнату? Изменить название? изменить дату?
                    CallbackQueryHandler(self._select_user_kick, pattern='^' + SELECT + '$'),
                    CallbackQueryHandler(self._change_room_token, pattern='^' + ROOM_TOKEN + '$'),
                    CallbackQueryHandler(self._return, pattern='^' + END + '$')
                ],
                SELECT: [
                    CallbackQueryHandler(self._kick_user, pattern='^(?!' + END + ').*$'),
                    CallbackQueryHandler(self._start_menu, pattern='^' + END + '$')
                ],
            },
            fallbacks=[
                CommandHandler(cmd_stop, self._stop_menu),
                CallbackQueryHandler(self._stop_menu, pattern='^' + END + '$')
            ],
            map_to_parent={
                END: back_state
            }
        )

        return handler

    def _start_conversation(self, update: Update, context: CallbackContext) -> str:
        """ Start room control conversation """
        self.user_presenter.validate_user(tg_id=update.effective_user.id,
                                          chat_id=update.effective_chat.id,
                                          name=update.effective_user.full_name,
                                          link=update.effective_user.link,
                                          link_name=update.effective_user.name)

        return self._start_menu(update, context)

    def _start_menu(self, update: Update, context: CallbackContext, text: str = '') -> str:
        """ Main room ctrl menu """
        tg_id = update.effective_user.id
        room = self._get_active_room(tg_id)
        if room and room.owner_id == tg_id:
            text += '👥 Активная комната:\nНазвание: {0}\nДата события: {1}\nТокен для приглашения:' \
                .format(room.title, room.date.date())
            buttons = [
                [InlineKeyboardButton(text='🚷 Исключить участника', callback_data=SELECT)],
                [InlineKeyboardButton(text='🔏 Изменить ключ комнаты', callback_data=ROOM_TOKEN)],
                # [InlineKeyboardButton(text='Изменить параметры комнаты', callback_data=ROOM_TOKEN)],
                [InlineKeyboardButton(text='⬅️ Назад', callback_data=END)],
            ]
            keyboard = InlineKeyboardMarkup(buttons)
            update.callback_query.answer()
            message = update.callback_query.edit_message_text(text=text)
            update.callback_query.message.reply_text(text=room.token, reply_markup=keyboard)
            context.user_data[DELETE_MSG] = message.message_id
        else:
            if room:
                text = '❗️ Вы не являетесь владельцем конматы {0}'.format(room.title)
            else:
                text = '❗️ У вас нет активной комнаты.'
            buttons = [[InlineKeyboardButton(text='⬅️ Назад', callback_data=END)]]
            keyboard = InlineKeyboardMarkup(buttons)
            update.callback_query.answer()
            update.callback_query.edit_message_text(text=text, reply_markup=keyboard)

        return MENU

    @staticmethod
    def _stop_menu(update: Update, context: CallbackContext) -> int:
        """ Stop room control conversation """
        context.user_data[START_OVER] = False
        update.callback_query.answer()
        update.callback_query.edit_message_text(text='До скорого!')
        return ConversationHandler.END

    def _return(self, update: Update, context: CallbackContext) -> str:
        if context.user_data[DELETE_MSG]:
            self.bot.delete_message(update.effective_chat.id, context.user_data[DELETE_MSG])

        context.user_data[DELETE_MSG] = None
        if self.back_func:
            self.back_func(update, context)
        return END

    def _select_user_kick(self, update: Update, context: CallbackContext, text: str = '') -> str:
        """ Select user for kick from room """
        room = self._get_active_room(update.effective_user.id)
        buttons = []
        for avatar in room.list:
            user = self.user_presenter.user(avatar.tg_id)
            if user and user.id != update.effective_user.id:
                buttons.append([InlineKeyboardButton(text=user.name, callback_data=str(avatar.tg_id))])

        buttons.append([InlineKeyboardButton(text='⬅️ Назад', callback_data=END)])
        keyboard = InlineKeyboardMarkup(buttons)

        text += 'Выберите участника для исключения и комнаты.' if len(buttons) > 1 else 'В комнате нет никого кроме вас!'

        update.callback_query.answer()
        update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        return SELECT

    def _kick_user(self, update: Update, context: CallbackContext) -> str:
        """ Kick user from room """
        removed_id = int(update.callback_query.data)
        removed_user = self.user_presenter.user(removed_id)
        room = self._get_active_room(update.effective_user.id)
        for avatar in room.list:
            if avatar.tg_id == removed_id and self.avatar_presenter.remove_avatar(avatar.id):
                text = '✅ Пользователь {0} успешно исключен.\n'.format(removed_user.name)
                break
        else:
            text = '❗️ Ошибка исключения пользователя!\n'

        return self._select_user_kick(update, context, text)

    def _change_room_token(self, update: Update, context: CallbackContext) -> str:
        """ Generate and change room token """
        user = self.user_presenter.user(update.effective_user.id)
        new_token = self.room_presenter.change_token(user.active_room_id)
        text = '✅ Токен успешно изменен!\n' if new_token else '❗️ Ошибка cмены токена\n'
        if context.user_data[DELETE_MSG]:
            self.bot.delete_message(update.effective_chat.id, context.user_data[DELETE_MSG])
        return self._start_menu(update, context, text)

    @staticmethod
    def _room_help(update: Update, context: CallbackContext) -> str:
        """ Get help info about room menu """
        text = ('Вы в меню управления комнатами. В этом меню вы можете управлять активной комнатой. Для управления '
                'комнатой необходимо являтся владельцем.\n'
                'Кнопка "🚷 Исключить участника" - позволяет исключить участника из команаты, чтобы участник не смог'
                'снова попасть в кмонату необходимо изменить ключ.\n '   
                'Кнопка "🔏 Изменить ключ комнаты" автоматически генерирует новый ключ для комнаты, старые'
                'ключи становятся недействительными.\n'
                'Кнопка "❓ Помощь" - откроет подсказку.\n'
                'Кнопка "⬅️ Назад" - вернутсья в предыдущее меню.')
        button = InlineKeyboardButton(text='⬅️ Назад', callback_data=END)
        keyboard = InlineKeyboardMarkup.from_button(button)

        update.callback_query.answer()
        update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        return HELP

    def _get_active_room(self, tg_id: int) -> Room or None:
        """ Get active room by telegram id """
        user = self.user_presenter.user(tg_id)
        if user and user.active_room_id:
            return self.room_presenter.room(user.active_room_id)
        else:
            return None
