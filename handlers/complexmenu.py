from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update
from telegram.ext import CallbackContext, CommandHandler, CallbackQueryHandler

from handlers.defines import *
from handlers.activeroommenu import ActiveRoomMenu
from handlers.changeroommenu import ChangeRoomMenu
from handlers.controlroommenu import ControlRoomMenu
from handlers.createroommenu import CreateRoomMenu


class ComplexMenu:
    def __init__(self, user_presenter, room_presenter, avatar_presenter, gift_presenter, bot):
        self.user_presenter = user_presenter
        self.active_room_menu = ActiveRoomMenu(user_presenter, room_presenter, avatar_presenter, gift_presenter, bot)
        self.create_room_menu = CreateRoomMenu(user_presenter, room_presenter, avatar_presenter, bot)
        self.control_room_menu = ControlRoomMenu(user_presenter, room_presenter, avatar_presenter, bot)
        self.change_room_menu = ChangeRoomMenu(user_presenter, room_presenter, avatar_presenter, bot)

    def get_conversation_handler(self, cmd_start: str, cmd_stop: str) -> ConversationHandler:

        active_handler = self.active_room_menu.get_conversation_handler(ROOM_MENU, cmd_stop, self._start_menu, MENU)
        create_handler = self.create_room_menu.get_conversation_handler(ROOM_CREATE, cmd_stop, self._start_menu, MENU)
        ctrl_handler = self.control_room_menu.get_conversation_handler(ROOM_CTRL, cmd_stop, self._start_menu, MENU)
        change_handler = self.change_room_menu.get_conversation_handler(ROOM_CHANGE, cmd_stop, self._start_menu, MENU)

        handler = ConversationHandler(
            entry_points=[CommandHandler(cmd_start, self._start_conversation)],
            states={
                MENU: [
                    active_handler,
                    create_handler,
                    ctrl_handler,
                    change_handler,
                    CallbackQueryHandler(self._help, pattern='^' + HELP + '$'),
                    CallbackQueryHandler(self._stop_menu, pattern='^' + END + '$')
                ],
                HELP: [
                    CallbackQueryHandler(self._about_bot, pattern='^' + ABOUT_BOT + '$'),
                    CallbackQueryHandler(self._about_buttons, pattern='^' + ABOUT_BUTTONS + '$'),
                    CallbackQueryHandler(self._start_menu, pattern='^' + END + '$')
                ],
                ABOUT_BOT: [CallbackQueryHandler(self._help, pattern='^' + END + '$')],
                ABOUT_BUTTONS: [CallbackQueryHandler(self._help, pattern='^' + END + '$')]
            },
            fallbacks=[CommandHandler(cmd_stop, self._stop_menu)],
            allow_reentry=True
        )

        return handler

    def _start_conversation(self, update: Update, context: CallbackContext) -> str:
        self.user_presenter.validate_user(tg_id=update.effective_user.id,
                                          chat_id=update.effective_chat.id,
                                          name=update.effective_user.full_name,
                                          link=update.effective_user.link,
                                          link_name=update.effective_user.name)

        context.user_data[START_OVER] = False
        return self._start_menu(update, context)

    @staticmethod
    def _start_menu(update: Update, context: CallbackContext) -> str:
        text = 'Стартовое меню Wishlist Bot 🎁.\n По кнопке "Помощь" есть подробное описание всех кнопок!\n' \
               'Желаю хорошо провести время!'
        buttons = [
            [InlineKeyboardButton(text='👥 Активная комната', callback_data=ROOM_MENU)],
            [InlineKeyboardButton(text='🆕 Создание комнаты', callback_data=ROOM_CREATE)],
            [InlineKeyboardButton(text='⚙️ Управление комнатой', callback_data=ROOM_CTRL)],
            [InlineKeyboardButton(text='🔁 Смена комнаты', callback_data=ROOM_CHANGE)],
            [InlineKeyboardButton(text='❓ Помощь', callback_data=HELP)],
            [InlineKeyboardButton(text='🚪 Выход', callback_data=END)]
        ]

        keyboard = InlineKeyboardMarkup(buttons)

        if context.user_data.get(START_OVER):
            update.callback_query.answer()
            update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        else:
            context.user_data[START_OVER] = True
            update.message.reply_text(text=text, reply_markup=keyboard)
        return MENU

    @staticmethod
    def _stop_menu(update: Update, context: CallbackContext) -> int:
        context.user_data[START_OVER] = False
        update.callback_query.answer()
        update.callback_query.edit_message_text(text='👋 До встречи! Для запуска нажмите /start')
        return ConversationHandler.END

    @staticmethod
    def _help(update: Update, context: CallbackContext) -> str:
        text = ('👋 Добро пожаловать в WishList Bot! Надеюсь вам понравится функционал, а я всегда готов откликнуться на '
                'идеи о его доработке!\nО чем бы вы хотели узнать подробнее?')

        buttons = [
            [InlineKeyboardButton(text='👁‍🗨 Подробнее о боте', callback_data=ABOUT_BOT)],
            [InlineKeyboardButton(text='💡 Назначение кнопок', callback_data=ABOUT_BUTTONS)],
            [InlineKeyboardButton(text='⬅️ Назад', callback_data=END)]
        ]

        keyboard = InlineKeyboardMarkup(buttons)

        update.callback_query.answer()
        update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        return HELP

    @staticmethod
    def _about_bot(update: Update, context: CallbackContext) -> str:
        text = ('Основная задача бота - помочь с организацией праздника. А именно с подарками. '
                'Существуют виртуальные пространства, которые я назвал комнатами. Каждый пользователь '
                'бота может создать новую или вступить в существующую комнату. Количество комнат не '
                'ограничено. Комната может иметь один из нескольких режимов.\n'
                '"Тайный санта" - простой режим, где комната наполняется участниками по приглашению, запускается '
                'распределение и каждому участнику случайным образом назначается другой участник для подарка.\n'
                '"Именнинник" - в этом режиме владелец комнаты имеет список подарков, который он может редактировать. '
                'Остальные участники могут смотреть этот список и бронировать подарки. Когда подарок занят все '
                'участники(кроме владельца) видят это и повторно забронировать не получится. Для владельца вишлиста '
                'все остается полностью анонимным(бронь подарка не отображается).\n'
                'Комнаты имеют ограниченный срок существования и после даты события будут удалены.\n'
                'Более подробное описание возможностей вы можете найти в по кнопке "❓ Помощь" в соответствующем меню!\n'
                'Приятного использования!')
        button = InlineKeyboardButton(text='⬅️ Назад', callback_data=END)
        keyboard = InlineKeyboardMarkup.from_button(button)
        update.callback_query.answer()
        update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        return ABOUT_BOT

    @staticmethod
    def _about_buttons(update: Update, context: CallbackContext) -> str:
        text = ('"👥 Активная комната" - меню взаимодействия с комнатой, которая у вас выбрана активной, в нем '
                'находятся действия связанные с режимом комнаты и взаимодействием с другими участниками.\n'
                '"🆕 Создание комнаты" - меню, которое позволит создать новую комнату, вы будете назначены владельцем '
                'этой комнаты.\n'
                '"⚙️ Управление комнатой" - меню, позволяющее управлять активной комнатой(необходимо быть владельцем). '
                'Позволяет изменить информацию о комнате, исключить участников и т.д.\n'
                '"🔁 Смена комнаты" - меню, позволяющее вступить в новую комнату по ключу-приглашению или изменить '
                'активную комнату на одну из тех в которых вы уже состоите.\n'
                'Более подробное описание вы можете найти, перейдя в соответствующее меню и выбрав кнопку "❓ Помощь".')
        button = InlineKeyboardButton(text='⬅️ Назад', callback_data=END)
        keyboard = InlineKeyboardMarkup.from_button(button)
        update.callback_query.answer()
        update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        return ABOUT_BUTTONS