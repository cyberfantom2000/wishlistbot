
class BaseRepository:
    def __init__(self, session) -> None:
        self.session = session

    def add_and_commit(self, orm_model) -> None:
        self.session.add(orm_model)
        self.session.commit()

    def create(self, model, to_orm_conv) -> None:
        self.add_and_commit(to_orm_conv(model))

    def get(self, orm_type, filters, from_orm_conv) -> list:
        query = self.session.query(orm_type).filter(*filters)
        return [from_orm_conv(i) for i in query]

    def remove(self, orm_type, orm_id) -> bool:
        orm = self.session.query(orm_type).filter(orm_type.id == orm_id).first()
        if orm:
            self.session.delete(orm)
            self.session.commit()
            return True
        else:
            return False
