"""
This module contains orm database models.
Model Room have relationship one-to-many to model User.
Models User and Gift have relationship many-to-many between them.
"""

from sqlalchemy import Column, Integer, String, ForeignKey, Table, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref

SqlOrmBase = declarative_base()


give_association_table = Table(
    'give_association_table',
    SqlOrmBase.metadata,
    Column('avatar_id', ForeignKey('avatars.id'), primary_key=True),
    Column('gift_id', ForeignKey('gifts.id'), primary_key=True)
)

# wish_association_table = Table(
#     'wish_association_table',
#     SqlOrmBase.metadata,
#     Column('avatar_id', ForeignKey('avatars.id'), primary_key=True),
#     Column('gift_id', ForeignKey('gifts.id'), primary_key=True)
# )

room_association_table = Table(
    'room_association_table',
    SqlOrmBase.metadata,
    Column('user_id', ForeignKey('users.id'), primary_key=True),
    Column('room_id', ForeignKey('rooms.id'), primary_key=True)
)


class GiftOrm(SqlOrmBase):
    """ Gift model.Has relationships with avatars """
    __tablename__ = 'gifts'
    id = Column(Integer, primary_key=True)
    title = Column(String(200), nullable=False)
    link = Column(String(400))
    description = Column(String(800))
    owner_id = Column(Integer, ForeignKey('avatars.id'))
    owner = relationship('AvatarOrm', foreign_keys=[owner_id],
                         backref=backref('wishlist', cascade='all, delete'))


class AvatarOrm(SqlOrmBase):
    """ Avatar model. User avatar in room. Has relationships with gifts """
    __tablename__ = 'avatars'
    id = Column(Integer, primary_key=True)
    tg_id = Column(Integer, nullable=False)
    target_avatar_id = Column(Integer) # for secret santa
    wishlist_ids = Column(Integer, ForeignKey('gifts.id'))
    givelist = relationship('GiftOrm', secondary=give_association_table, backref='givers')
    room_id = Column(Integer, ForeignKey('rooms.id'))
    room = relationship('RoomOrm', foreign_keys=[room_id], uselist=False,
                        backref=backref('avatars', cascade='all, delete'))


class RoomOrm(SqlOrmBase):
    """ Room model. Has relationships with avatars and users """
    __tablename__ = 'rooms'
    id = Column(Integer, primary_key=True)
    title = Column(String(200), nullable=False)
    invite_token = Column(String, nullable=False, unique=True)
    event_date = Column(DateTime, nullable=False)
    regime = Column(String, nullable=False, default='Common')
    owner_id = Column(Integer, ForeignKey('users.id'))
    owner = relationship('UserOrm', foreign_keys=[owner_id], uselist=False,
                         backref=backref('self_rooms', cascade='all, delete'))


class UserOrm(SqlOrmBase):
    """ User model. Has relationships with rooms """
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True) # telegramm id
    chat_id = Column(Integer, nullable=False)
    name = Column(String)
    link = Column(String)
    link_name = Column(String)
    active_room_id = Column(Integer, ForeignKey('rooms.id'))
    active_room = relationship('RoomOrm', foreign_keys=[active_room_id], uselist=False)
    active_avatar_id = Column(Integer, ForeignKey('avatars.id'))
    active_avatar = relationship('AvatarOrm', foreign_keys=[active_avatar_id], uselist=False)
    rooms = relationship('RoomOrm', secondary=room_association_table, backref='users')


