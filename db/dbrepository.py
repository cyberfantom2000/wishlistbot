from core.user import IUserStorage, UserFilter
from core.avatar import IAvatarStorage, AvatarFilter
from core.room import IRoomStorage, RoomFilter
from core.gift import IGiftStorage, GiftFilter
from core.entities import Room, Avatar, User, Gift
from db.models import UserOrm, RoomOrm, AvatarOrm, GiftOrm
from db.dbbase import BaseRepository
from sqlalchemy import or_


class DbRepository(IUserStorage, IRoomStorage, IAvatarStorage, IGiftStorage):
    """ Database repository """
    def __init__(self, base_repo: BaseRepository):
        self.base_repo = base_repo

    def create_user(self, user: User) -> bool:
        """ Add user to db """
        self.base_repo.create(user, self.__user_to_orm)
        return True

    def update_user(self, user: User) -> bool:
        """ Update user data in db """
        user_orm = self.base_repo.session.query(UserOrm).filter(UserOrm.id == user.id).first()
        if not user_orm:
            return False

        user_orm.chat_id = user.chat_id
        user_orm.name = user.name
        user_orm.link = user.link
        user_orm.link_name = user.link_name
        user_orm.active_room_id = user.active_room_id
        user_orm.active_avatar_id = user.active_avatar.id if user.active_avatar else None
        if user.room_ids:
            filters = [RoomOrm.id == room_id for room_id in user.room_ids]
            user_orm.list = self.base_repo.session.query(RoomOrm).filter(or_(*filters)).all()
        self.base_repo.add_and_commit(user_orm)
        return True

    def users(self, filters: UserFilter) -> list[User]:
        """ Get user from db """
        db_filters = []
        if filters.tg_id is not None:
            db_filters.append(UserOrm.id == filters.tg_id)
        if filters.active_room_id is not None:
            db_filters.append(UserOrm.active_room_id == filters.active_room_id)

        return self.base_repo.get(UserOrm, db_filters, self.__user_from_orm)

    def create_room(self, room: Room) -> bool:
        """ Create new room """
        self.base_repo.create(room, self.__room_to_orm)
        return True

    def remove_room(self, room_id: int) -> bool:
        """ Remove room from db """
        return self.base_repo.remove(RoomOrm, room_id)

    def rooms(self, filters: RoomFilter) -> list[Room]:
        """ Get rooms from db """
        db_filters = []
        if filters.owner_id is not None:
            db_filters.append(RoomOrm.owner_id == filters.owner_id)
        if filters.room_ids is not None:
            room_filters = [RoomOrm.id == room_id for room_id in filters.room_ids]
            db_filters.append(or_(*room_filters))
        if filters.token is not None:
            db_filters.append(RoomOrm.invite_token == filters.token)
        return self.base_repo.get(RoomOrm, db_filters, self.__room_from_orm)

    def update_room(self, room: Room) -> bool:
        """ Update room fields into db """
        room_orm = self.base_repo.session.query(RoomOrm).filter(RoomOrm.id == room.id).first()
        if not room_orm:
            return False

        room_orm.title = room.title
        room_orm.regime = room.regime
        room_orm.invite_token = room.token
        room_orm.event_date = room.date
        room_orm.owner_id = room.owner_id
        self.base_repo.add_and_commit(room_orm)
        return True

    def create_avatar(self, avatar: Avatar) -> bool:
        """ Create new avatar """
        self.base_repo.create(avatar, self.__avatar_to_orm)
        return True

    def remove_avatar(self, avatar_id: int) -> bool:
        """ Remove avatar from db """
        return self.base_repo.remove(AvatarOrm, avatar_id)

    def avatars(self, filters: AvatarFilter) -> list[Avatar]:
        """ Get avatars from db """
        db_filters = []
        if filters.room_id is not None:
            db_filters.append(AvatarOrm.room_id == filters.room_id)
        if filters.tg_id:
            db_filters.append(AvatarOrm.tg_id == filters.tg_id)
        if filters.avatar_id:
            db_filters.append(AvatarOrm.id == filters.avatar_id)
        return self.base_repo.get(AvatarOrm, db_filters, self.__avatar_from_orm)

    def update_avatar(self, avatar: Avatar) -> bool:
        """ Update avatar fields into db """
        avatar_orm = self.base_repo.session.query(AvatarOrm).filter(AvatarOrm.id == avatar.id).first()
        if not avatar_orm:
            return False

        avatar_orm.tg_id = avatar.tg_id
        avatar_orm.target_avatar_id = avatar.target_avatar_id
        if avatar.wish_list_ids:
            avatar_orm.wishlist_ids = avatar.wish_list_ids
        if avatar.give_list_ids:
            avatar_orm.givelist_ids = avatar.give_list_ids
        avatar_orm.room_id = avatar.room_id
        self.base_repo.add_and_commit(avatar_orm)
        return True

    def create_gift(self, gift: Gift) -> bool:
        self.base_repo.create(gift, self.__gift_to_orm)
        return True

    def remove_gift(self, gift_id: int) -> bool:
        self.base_repo.remove(GiftOrm, gift_id)
        return True

    def update_gift(self, gift: Gift) -> bool:
        gift_orm = self.base_repo.session.query(GiftOrm).filter(GiftOrm.id == gift.id).first()
        if not gift_orm:
            return False
        gift_orm.link = gift.link
        gift_orm.title = gift.title
        gift_orm.description = gift.description
        if gift.giver_ids:
            filters = [AvatarOrm.id == giver_id for giver_id in gift.giver_ids]
            gift_orm.givers = self.base_repo.session.query(AvatarOrm).filter(or_(*filters)).all()
        else:
            gift_orm.givers.clear()

        self.base_repo.add_and_commit(gift_orm)
        return True

    def gifts(self, filters: GiftFilter) -> list[Gift]:
        db_filters = []
        if filters.gift_id:
            db_filters.append(GiftOrm.id == filters.gift_id)
        if filters.owner_id:
            db_filters.append(GiftOrm.owner_id == filters.owner_id)
        if filters.giver_id:
            raise  # TODO

        return self.base_repo.get(GiftOrm, db_filters, self.__gift_from_orm)

    def __user_to_orm(self, user: User) -> UserOrm:
        orm = UserOrm(id=user.id,
                      chat_id=user.chat_id,
                      name=user.name,
                      link=user.link,
                      link_name=user.link_name,
                      active_room_id=user.active_room_id)

        if user.active_avatar:
            orm.active_avatar_id = user.active_avatar.id

        if user.room_ids:
            filters = [RoomOrm.id == room_id for room_id in user.room_ids]
            orm.rooms = self.base_repo.session.query(RoomOrm).filter(or_(*filters)).all()
        else:
            orm.rooms = []

        return orm

    def __user_from_orm(self, user_orm: UserOrm) -> User:
        if not user_orm:
            user = None
        else:
            user = User(id=user_orm.id,
                        chat_id=user_orm.chat_id,
                        name=user_orm.name,
                        link=user_orm.link,
                        link_name=user_orm.link_name,
                        active_room_id=user_orm.active_room_id,
                        active_avatar=self.__avatar_from_orm(user_orm.active_avatar),
                        room_ids=[room.id for room in user_orm.rooms])
        return user

    def __room_to_orm(self, room: Room) -> RoomOrm:
        orm = RoomOrm(id=room.id,
                      title=room.title,
                      invite_token=room.token,
                      event_date=room.date,
                      regime=room.regime,
                      owner_id=room.owner_id)
        if room.avatars:
            for avatar in room.avatars:
                orm_avatar = self.__avatar_to_orm(avatar)
                orm.avatars.append(orm_avatar)
        return orm

    def __room_from_orm(self, room_orm: RoomOrm) -> Room:
        if not room_orm:
            room = None
        else:
            avatars = [self.__avatar_from_orm(avatar) for avatar in room_orm.avatars] if room_orm.avatars else []
            room = Room(id=room_orm.id,
                        title=room_orm.title,
                        token=room_orm.invite_token,
                        date=room_orm.event_date,
                        regime=room_orm.regime,
                        owner_id=room_orm.owner_id,
                        avatars=avatars)
        return room

    def __avatar_to_orm(self, avatar: Avatar) -> AvatarOrm:
        orm = AvatarOrm(
            id=avatar.id,
            tg_id=avatar.tg_id,
            room_id=avatar.room_id,
            target_avatar_id=avatar.target_avatar_id
        )
        if avatar.wish_list_ids:
            query = self.base_repo.session.query(Gift).filters(Gift.id in avatar.wish_list_ids)
            orm.wishlist.append(query)
        if avatar.give_list_ids:
            query = self.base_repo.session.query(Gift).filters(Gift.id in avatar.give_list_ids)
            orm.wishlist.append(query)
        return orm

    @staticmethod
    def __avatar_from_orm(avatar_orm: AvatarOrm) -> Avatar or None:
        if not avatar_orm:
            return None
        avatar = Avatar(id=avatar_orm.id,
                        tg_id=avatar_orm.tg_id,
                        room_id=avatar_orm.room_id,
                        target_avatar_id=avatar_orm.target_avatar_id,
                        wish_list_ids=[gift.id for gift in avatar_orm.wishlist] if avatar_orm.wishlist else [],
                        give_list_ids=[gift.id for gift in avatar_orm.givelist] if avatar_orm.givelist else [])
        return avatar

    @staticmethod
    def __gift_to_orm(gift: Gift) -> GiftOrm:
        orm = GiftOrm(
            id=gift.id,
            owner_id=gift.avatar_id,
            title=gift.title,
            link=gift.link,
            description=gift.description,
        )
        if gift.giver_ids:
            orm.givers = gift.giver_ids

        return orm

    @staticmethod
    def __gift_from_orm(gift_orm: GiftOrm) -> Gift or None:
        if not gift_orm:
            return None
        gift = Gift(id=gift_orm.id,
                    avatar_id=gift_orm.owner_id,
                    title=gift_orm.title,
                    link=gift_orm.link,
                    description=gift_orm.description,
                    giver_ids=[giver.id for giver in gift_orm.givers] if gift_orm.givers else [])
        return gift
