import os
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from db.models import SqlOrmBase

if not os.path.exists('wishlist.db'):
    open('wishlist.db', 'w+').close()

db_engine = create_engine('sqlite:///wishlist.db')
SqlOrmBase.metadata.create_all(db_engine)
db_session = Session(bind=db_engine)
