from telegram.ext import Updater, PicklePersistence
import logging

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

with open('token.txt', 'r') as file:
    persistence = PicklePersistence(
        filename='arbitrarycallbackdatabot.pickle', store_callback_data=True
    )
    updater = Updater(file.readline().strip(), persistence=persistence, arbitrary_callback_data=True)

dispatcher = updater.dispatcher
bot = updater.bot