from dataclasses import dataclass
from typing import Optional
from abc import ABC, abstractmethod
from core.entities import Gift


@dataclass
class GiftFilter:
    gift_ids: Optional[list[int]] = None
    gift_id: Optional[int] = None
    owner_id: Optional[int] = None
    giver_id: Optional[int] = None


class IGiftStorage(ABC):
    @abstractmethod
    def create_gift(self, gift: Gift) -> bool:
        pass

    def remove_gift(self, gift_id: int) -> bool:
        pass

    @abstractmethod
    def update_gift(self, gift: Gift) -> bool:
        pass

    @abstractmethod
    def gifts(self, filters: GiftFilter) -> list[Gift]:
        pass


class GiftManager:
    def __init__(self, repo: IGiftStorage):
        self.repo = repo

    def create(self, avatar_id: int,  title: str, link: str = '', description: str = '') -> bool:
        new_gift = Gift(avatar_id=avatar_id, title=title, link=link, description=description)
        return self.repo.create_gift(new_gift)

    def update(self, gift: Gift) -> bool:
        return self.repo.update_gift(gift) if gift.id else False

    def remove_gift(self, gift_id: int) -> bool:
        return self.repo.remove_gift(gift_id)

    def gifts(self, ids: list[int]) -> list[Gift]:
        return self.repo.gifts(GiftFilter(gift_ids=ids))
