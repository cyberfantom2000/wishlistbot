from dataclasses import dataclass
from abc import ABC, abstractmethod
from core.entities import User, Avatar
from typing import Optional


@dataclass
class UserFilter:
    tg_id: Optional[int] = None
    active_room_id: Optional[int] = None


class IUserStorage(ABC):
    """ User repository interface"""

    @abstractmethod
    def create_user(self, user: User) -> bool:
        """ Add new user to storage """
        pass

    @abstractmethod
    def update_user(self, user: User) -> bool:
        """ Update user into storage """
        pass

    @abstractmethod
    def users(self, filters: UserFilter) -> list[User]:
        """ Get user from storage"""
        pass


class UserManager:
    """ User interaction """

    def __init__(self, repo: IUserStorage):
        self.repo = repo

    def create(self, tg_id: int, chat_id: int, name: str, link: str, link_name: str) -> User or None:
        """ Add ne user to storage """
        new_user = User(chat_id=chat_id,
                        id=tg_id,
                        name=name,
                        link=link,
                        link_name=link_name)
        if self.repo.create_user(new_user):
            return self.list(UserFilter(tg_id=tg_id))[0]
        else:
            return None

    def update(self, user: User) -> bool:
        """ Update user to storage """
        if not user.id:
            # TODO add log
            return False
        else:
            return self.repo.update_user(user)

    def remove(self, tg_id: int) -> bool:
        """ Remove room by id """
        users = self.list(UserFilter(tg_id=tg_id))
        return False if not users else self.repo.remove_user(tg_id)

    def list(self, filters: UserFilter) -> list[User]:
        return self.repo.users(filters)
