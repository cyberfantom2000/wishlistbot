from core.room import RoomManager, RoomFilter
from datetime import datetime


class RoomDeleter:
    """ Checks the date of the event in all rooms and removes expired rooms """
    def __init__(self, room_manager: RoomManager) -> None:
        self.room_manager = room_manager

    def delete_expired_rooms(self) -> None:
        """ Start validate rooms date """
        rooms = self.room_manager.list(RoomFilter())
        cur_date = datetime.now()
        for room in rooms:
            if (room.date - cur_date).days < 0:
                if not self.room_manager.remove(room.id):
                    pass # TODO add log