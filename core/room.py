from dataclasses import dataclass
from abc import ABC, abstractmethod
from core.entities import Room
from typing import Optional
from datetime import datetime
import secrets


@dataclass
class RoomFilter:
    room_ids: Optional[list[int]] = None
    token: Optional[str] = None
    owner_id: Optional[int] = None


class IRoomStorage(ABC):
    """ Room repository interface"""
    @abstractmethod
    def create_room(self, room: Room) -> bool:
        pass

    @abstractmethod
    def remove_room(self, room_id: int) -> bool:
        pass

    @abstractmethod
    def rooms(self, filters: RoomFilter) -> list[Room]:
        pass

    @abstractmethod
    def update_room(self, room: Room) -> bool:
        pass


class RoomManager:
    """ Room interaction """
    def __init__(self, repo: IRoomStorage):
        self.repo = repo

    def create(self, tg_id: int, title: str, regime: str, event_date: datetime) -> Room or None:
        """ Create new room """
        new_room = Room(title=title,
                        token=str(secrets.token_hex(16)),
                        date=event_date,
                        regime=regime,
                        owner_id=tg_id)

        if self.repo.create_room(new_room):
            return self.list(RoomFilter(token=new_room.token))[0]
        else:
            return None

    def update(self, room: Room) -> bool:
        """ Add new room to storage. Return status. """
        return self.repo.update_room(room) if room.id else False

    def remove(self, room_id: int) -> bool:
        """ Remove room by id """
        rooms = self.list(RoomFilter(room_ids=[room_id]))
        return False if not rooms else self.repo.remove_room(room_id)

    def list(self, filters: RoomFilter) -> list[Room]:
        """ Return room list by RoomFilter """
        return self.repo.rooms(filters)
