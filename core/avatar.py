from abc import ABC, abstractmethod
from core.entities import Avatar
from dataclasses import dataclass
from typing import Optional


@dataclass
class AvatarFilter:
    tg_id: Optional[int] = None
    room_id: Optional[int] = None
    avatar_id: Optional[int] = None


class IAvatarStorage(ABC):
    @abstractmethod
    def create_avatar(self, avatar: Avatar) -> bool:
        pass

    @abstractmethod
    def remove_avatar(self, avatar_id: int) -> bool:
        pass

    @abstractmethod
    def avatars(self, filters: AvatarFilter) -> list[Avatar]:
        pass

    @abstractmethod
    def update_avatar(self, avatar: Avatar) -> bool:
        pass


class AvatarManager:
    """ Avatar interaction """
    def __init__(self, repo: IAvatarStorage):
        self.repo = repo

    def create(self, tg_id: int, room_id: int) -> Avatar:
        """ Add new avatar to storage and return him or just return exists avatar. """
        avatars = self.list(AvatarFilter(tg_id=tg_id, room_id=room_id))
        if avatars:
            return avatars[0]
        else:
            self.repo.create_avatar(Avatar(tg_id=tg_id, room_id=room_id))
            avatars = self.list(AvatarFilter(tg_id=tg_id, room_id=room_id))
            return None if not avatars else avatars[0]

    def update(self, avatar: Avatar) -> bool:
        """ Update avatar into storage """
        if not avatar.id:
            return False
        else:
            return self.repo.update_avatar(avatar)

    def remove(self, avatar_id: int) -> bool:
        """ Remove avatar from storage. """
        return self.repo.remove_avatar(avatar_id)

    def list(self, filters: AvatarFilter) -> list[Avatar]:
        """ Get avatar list by AvatarFilter """
        return self.repo.avatars(filters)
