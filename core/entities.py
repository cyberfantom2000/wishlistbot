from dataclasses import dataclass
from typing import Optional
from datetime import datetime


@dataclass
class Gift:
    avatar_id: int  # id владеющего аватара
    id: Optional[int] = None
    title: Optional[str] = None
    link: Optional[str] = None
    description: Optional[str] = None
    giver_ids: Optional[list[int]] = None


@dataclass
class Avatar:
    tg_id: int
    id: Optional[int] = None
    target_avatar_id: Optional[int] = None
    room_id: Optional[int] = None
    wish_list_ids: Optional[list[int]] = None
    give_list_ids: Optional[list[int]] = None


@dataclass
class Room:
    title: str
    token: str
    date: datetime
    regime: str
    owner_id: int
    id: Optional[int] = None
    avatars: Optional[list[Avatar]] = None


@dataclass
class User:
    id: int  # telegram id
    chat_id: int
    name: str
    link: str
    link_name: str  # telegram link name
    active_room_id: Optional[int] = None
    active_avatar: Optional[Avatar] = None
    room_ids: Optional[list[int]] = None








